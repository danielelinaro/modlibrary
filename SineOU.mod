COMMENT
Noisy sinusoidal current

Author: Daniele Linaro
Date: January 21, 2010
ENDCOMMENT

NEURON {
    POINT_PROCESS SineOU
    RANGE i, iaux
    RANGE delay, dur
    RANGE a, f, phi
    RANGE mu, sigma, tau
    : k and sigmax are needed only when the exact method of solution is used
    RANGE sigmax, k
    RANGE seed, pi
    ELECTRODE_CURRENT i
    THREADSAFE
}

VERBATIM
#define SINEOU _sineou
typedef unsigned long long ullong;
ullong jSINEOU, uSINEOU, wSINEOU, zSINEOU;
double storedvalSINEOU;
ENDVERBATIM

UNITS {
    (nA) = (nanoamp)
}

PARAMETER {
    delay= 0  (ms)
    dur       (ms)
    a    = 1  (nA)
    f    = 10 (/s)
    phi  = 0  (/s)
    mu   = 0  (nA)
    sigma= 0  (nA)
    tau  = 10 (ms)
    seed = 5061983
    pi = 3.1415926535897931
}

FUNCTION int64() {
    VERBATIM
    {
	uSINEOU = uSINEOU * 2862933555777941757LL + 7046029254386353087LL; 
	wSINEOU ^= wSINEOU >> 17;
	wSINEOU ^= wSINEOU << 31;
	wSINEOU ^= wSINEOU >> 8; 
	zSINEOU = 4294957665U*(zSINEOU & 0xffffffff) + (zSINEOU >> 32); 
	ullong x = uSINEOU ^ (uSINEOU << 21); x ^= x >> 35; x ^= x << 4; 
	return (x + wSINEOU) ^ zSINEOU;
    }
    ENDVERBATIM
}

ASSIGNED {
    i    (nA)
    iaux (nA)
    dt   (ms)
    : k and sigmax are needed only when the exact method of solution is used
    sigmax (nA)
    k      (1)
}

INITIAL {
    i = 0
    iaux = 0
    : k and sigmax are needed only when the exact method of solution is used
    k = exp(-dt/tau)
    sigmax = sqrt(sigma*sigma*(1-k*k))
    :sigmax = sqrt((sigma*sigma*tau/2)*(1-k*k))
    rand_init(seed)
}

PROCEDURE rand_init(seed) {
    VERBATIM
    jSINEOU = (ullong) seed;
    wSINEOU = 4101842887655102017LL;
    zSINEOU = 1;
    uSINEOU = jSINEOU ^ wSINEOU; int64();
    wSINEOU = uSINEOU; int64();
    zSINEOU = wSINEOU; int64();
    storedvalSINEOU = 0.0;
    ENDVERBATIM
}

FUNCTION doub() {
    VERBATIM
    return 5.42101086242752217E-20 * int64();
    ENDVERBATIM
}

FUNCTION normdeviate() {
    VERBATIM
    {
	/* returns a random normal number with 0 mean and unitary variance */
	double v1,v2,rsq,fac; 
	if (storedvalSINEOU == 0.) {
	    do {
		v1 = 2.0*doub()-1.0;
		v2 = 2.0*doub()-1.0; 
		rsq=v1*v1+v2*v2;
	    } while (rsq >= 1.0 || rsq == 0.0); 
	    fac = sqrt(-2.0*log(rsq)/rsq);
	    storedvalSINEOU = v1*fac; 
	    return v2*fac;
	} else {
	    fac = storedvalSINEOU; 
	    storedvalSINEOU = 0.; 
	    return fac;
	} 
    }
    ENDVERBATIM
}

BREAKPOINT {
    at_time(delay)
    at_time(delay+dur)
    if (t<delay+dur && t>delay) {
	: Exact solution
	iaux = iaux*k + sigmax * normdeviate()
	: Euler
	:iaux = (1-dt/tau)*iaux + sqrt(2/tau*dt) * sigma * normdeviate()
	i = a * sin( (2*pi*f+phi) * t/1000) + mu + iaux
    }
    else {
	i = 0
    }
}

