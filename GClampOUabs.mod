COMMENT
Dynamic clamp that generates a Ornstein-Uhlenbec random conductance.

Author: Daniele Linaro - daniele.linaro@unige.it
Date: September 16th 2010
ENDCOMMENT

UNITS {
    (mV) = (millivolt)
    (nA) = (nanoamp)
    (S)  = (siemens)
    (um) = (micron)
}

NEURON {
    POINT_PROCESS GClampOUabs
    RANGE g, gaux
    RANGE e
    RANGE delay, dur
    RANGE mu, sigma, tau
    RANGE sigmax, k
    RANGE seed
    ELECTRODE_CURRENT i
    THREADSAFE
}

VERBATIM
typedef unsigned long long ullong;
ullong j, u, w, z;
double storedval;
ENDVERBATIM

PARAMETER {
    e              (mV)
    delay          (ms)
    dur            (ms)
    mu             (S)
    sigma          (S)
    tau  = 10      (ms)
    seed = 5061983 (1)
}

FUNCTION int64() {
    VERBATIM
    {
	u = u * 2862933555777941757LL + 7046029254386353087LL; 
	w ^= w >> 17;
	w ^= w << 31;
	w ^= w >> 8; 
	z = 4294957665U*(z & 0xffffffff) + (z >> 32); 
	ullong x = u ^ (u << 21); x ^= x >> 35; x ^= x << 4; 
	return (x + w) ^ z;
    }
    ENDVERBATIM
}

ASSIGNED {
    gaux   (S)
    g      (S)
    i      (nA)
    dt     (ms)
    sigmax (S)
    k      (1)
    v      (mV)
}

INITIAL {
    UNITSOFF
    gaux = 0
    g = 0
    k = exp(-dt/tau)
    sigmax = sqrt(sigma*sigma*(1-k*k))
    rand_init(seed)
    UNITSON
}

PROCEDURE rand_init(seed) {
    VERBATIM
    j = (ullong) seed;
    w = 4101842887655102017LL;
    z = 1;
    u = j ^ w; int64();
    w = u; int64();
    z = w; int64();
    storedval = 0.0;
    ENDVERBATIM
}

FUNCTION doub() {
    VERBATIM
    return 5.42101086242752217E-20 * int64();
    ENDVERBATIM
}

FUNCTION normdeviate() {
    VERBATIM
    {
	/* returns a random normal number with 0 mean and unitary variance */
	double v1,v2,rsq,fac; 
	if (storedval == 0.) {
	    do { 
		v1 = 2.0*doub()-1.0;
		v2 = 2.0*doub()-1.0; 
		rsq=v1*v1+v2*v2;
	    } while (rsq >= 1.0 || rsq == 0.0); 
	    fac = sqrt(-2.0*log(rsq)/rsq);
	    storedval = v1*fac; 
	    return v2*fac;
	} else {
	    fac = storedval; 
	    storedval = 0.; 
	    return fac;
	} 
    }
    ENDVERBATIM
}

BREAKPOINT {
    at_time(delay)
    at_time(delay+dur)
    if (t>delay && t<delay+dur) {
	gaux = gaux*k + sigmax * normdeviate()
	g = mu + gaux
	i = (1e+06)*g*(v-e)
    } else {
	g = 0
	i = 0
    }
}

