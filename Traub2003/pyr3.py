#!/usr/bin/env python

from neuron import h

class L23Pyramidal:
    def __init__(self, dnap=0.0, dkc=1.6):
        h.load_file('l23pyr_template')
        n = 1
        while True:
            try:
                name = 'cell_%03d' % n
                getattr(h, name)
                n = n+1
            except:
                h('objref ' + name)
                h(name + ' = new l23pyr(%f,%f)' % (dnap,dkc))
                break
        self._cell = getattr(h, name)
        self.nSections = len(self._cell.comp)

    @property
    def soma(self):
        return self._cell.Soma.Section(0)

    @property
    def sections(self):
        return self._cell.comp

if __name__ == '__main__':
    import pylab as p

    dnap = 0.0
    dkc = 1.3
    cell = L23Pyramidal(dnap, dkc)
    inj1 = h.IClamp(cell.sections[1](0.5))
    inj1.dur = 150
    inj1.delay = 0
    inj1.amp = -0.15
    inj2 = h.IClamp(cell.sections[1](0.5))
    inj2.dur = 250
    inj2.delay = 150
    inj2.amp = 2.5
    
    rec = {}
    for lbl in 't','v':
        rec[lbl] = h.Vector()
    rec['t'].record(h._ref_t)
    rec['v'].record(cell.sections[1](0.5)._ref_v)

    h.load_file('stdrun.hoc')
    h.tstop = 450
    h.dt = 0.025
    h.celsius = 36
    h.finitialize(-71)
    h.run()
    
    p.figure()
    p.plot(rec['t'],rec['v'],'k',label='Soma')
    p.xlabel('Time (ms)')
    p.ylabel('Voltage (mV)')
    p.legend(loc='best')
    p.show()



