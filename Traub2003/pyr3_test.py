#!/usr/bin/env python

from neuron import h

dt = 0.025
tstop = 450
fig = 42
v_init = -70

h.load_file('pyr3_template')
h('objref cell')
h('cell = new pyr3(%d)' % fig)

rec = {}
for lbl in 't','v':
    rec[lbl] = h.Vector()
rec['t'].record(h._ref_t)
rec['v'].record(h.cell.comp[1](0.5)._ref_v)

h.load_file('stdrun.hoc')
h.dt = dt
h.tstop = tstop
h.finitialize(v_init)
h.run()

import pylab as p
p.plot(rec['t'],rec['v'],'k')
p.xlabel('Time (ms)')
p.ylabel('Voltage (mV)')
p.show()

