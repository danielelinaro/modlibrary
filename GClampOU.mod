COMMENT
Dynamic clamp that generates a Ornstein-Uhlenbeck random conductance.

Author: Daniele Linaro - daniele.linaro@unige.it
Date: September 16th 2010
ENDCOMMENT

UNITS {
    (mV) = (millivolt)
    (nA) = (nanoamp)
    (S)  = (siemens)
    (nS)  = (nanosiemens)
    (um) = (micron)
}

NEURON {
    POINT_PROCESS GClampOU
    RANGE g, g0
    RANGE e
    RANGE delay, dur, tau, sigma
    RANGE mu, D, coeff
    RANGE seed
    ELECTRODE_CURRENT i
    THREADSAFE
}

PARAMETER {
    e              (mV)
    g0             (nS)
    sigma          (nS)
    delay          (ms)
    dur            (ms)
    tau  = 10      (ms)
    seed = 12345   (1)
}

ASSIGNED {
    mu     (nS)
    D      (nS2/ms)
    coeff  (nS)
    g      (nS)
    i      (nA)
    dt     (ms)
    v      (mV)
}

INITIAL {
    UNITSOFF
    g = 0
    mu = exp(-dt/tau)
    D = 2*sigma*sigma/tau
    coeff = sqrt(D*tau/2 * (1-mu*mu))
    rand_init(seed)
    :printf("Set seed %.0f\n", seed)
    UNITSON
}

BREAKPOINT {
    at_time(delay)
    at_time(delay+dur)
    if (t>delay && t<delay+dur) {
	g = g0*(1-mu) + mu*g + coeff*normdeviate(0,1)
	i = (g*1e-3)*(e-v)
    } else {
	g = 0
	i = 0
    }
}

VERBATIM
#ifndef DL_RANDOM
#define DL_RANDOM

typedef unsigned long long ullong;
typedef struct {
        ullong j, u, w, z;
        double storedval;
} norm_random;

#endif

norm_random gclampou_rand;
#define j gclampou_rand.j
#define u gclampou_rand.u
#define w gclampou_rand.w
#define z gclampou_rand.z
#define storedval gclampou_rand.storedval

ENDVERBATIM

FUNCTION int64() {
    VERBATIM
    {
	u = u * 2862933555777941757LL + 7046029254386353087LL; 
	w ^= w >> 17;
	w ^= w << 31;
	w ^= w >> 8; 
	z = 4294957665U*(z & 0xffffffff) + (z >> 32); 
	ullong x = u ^ (u << 21); x ^= x >> 35; x ^= x << 4; 
	return (x + w) ^ z;
    }
    ENDVERBATIM
}

PROCEDURE rand_init(seed) {
    VERBATIM
    j = (ullong) seed;
    w = 4101842887655102017LL;
    z = 1;
    u = j ^ w; int64();
    w = u; int64();
    z = w; int64();
    storedval = 0.0;
    ENDVERBATIM
}

FUNCTION doub() {
    VERBATIM
    return 5.42101086242752217E-20 * int64();
    ENDVERBATIM
}

FUNCTION normdeviate() {
    VERBATIM
    {
	/* returns a random normal number with 0 mean and unitary variance */
	double v1,v2,rsq,fac; 
	if (storedval == 0.) {
	    do { 
		v1 = 2.0*doub()-1.0;
		v2 = 2.0*doub()-1.0; 
		rsq=v1*v1+v2*v2;
	    } while (rsq >= 1.0 || rsq == 0.0); 
	    fac = sqrt(-2.0*log(rsq)/rsq);
	    storedval = v1*fac; 
	    return v2*fac;
	} else {
	    fac = storedval; 
	    storedval = 0.; 
	    return fac;
	} 
    }
    ENDVERBATIM
}


