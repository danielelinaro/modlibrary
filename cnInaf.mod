
TITLE Fast inactivating sodium current with channel noise (Markov chain version)

COMMENT

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: March 2012

ENDCOMMENT

UNITS {
    (mA) = (milliamp)
    (mV) = (millivolt)
    (S)  = (siemens)
    (pS) = (picosiemens)
    (um) = (micron)
}

NEURON {
    SUFFIX cnInaf
    USEION na READ ena WRITE ina
    RANGE gnabar, gna, gammana, Nna
    RANGE seed
    THREADSAFE
}

DEFINE CNNAF_NSTATES         8
DEFINE CNNAF_NSTATES_SQUARE 64
DEFINE CNNAF_OPEN            7

PARAMETER {
    gnabar  = 0.12    (S/cm2)    : maximum sodium conductance
    gammana = 10      (pS)	 : single channel sodium conductance
    seed    = 5061983 (1)        : always use the same seed
}

STATE {
    spam
    channels[CNNAF_NSTATES]        (1)
}


ASSIGNED {
    ina    (mA/cm2)
    gna    (S/cm2)
    ena    (mV)
    dt     (ms)
    area   (um2)
    v      (mV)
    Nna    (1)
    channels_now[CNNAF_NSTATES]     (1)
    A[CNNAF_NSTATES_SQUARE]         (1)
}

INITIAL {
    spam = 0
    Nna = ceil(((1e-8)*area)*(gnabar)/((1e-12)*gammana))
    printf("[Inaf] number of channels = %.0f.\n", Nna)
    FROM i=0 TO CNNAF_NSTATES-1 {
	channels[i] = 0
    }
    channels[4] = Nna
    set_seed(seed)
}


BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = channels[CNNAF_OPEN]*((1e-12)*gammana)/((1e-8)*area)
    ina = gna * (v-ena)
}


DERIVATIVE states {
    spam' = spam
    fill_transition_matrix()
    evolve_channels()
}


PROCEDURE evolve_channels() {
    LOCAL escape_rate,transition_probability,fraction,nchannels,rnd
    FROM i=0 TO CNNAF_NSTATES-1 {
	channels_now[i] = channels[i]
    }
    
    FROM i=0 TO CNNAF_NSTATES-1 {
	escape_rate = 0
	FROM j=0 TO CNNAF_NSTATES-1 {
	    if (i != j) {
		escape_rate = escape_rate + A[i*CNNAF_NSTATES+j]
	    }
	}
	transition_probability = dt*escape_rate : probability of changing state
	nchannels = channels_now[i]
	FROM n=1 TO nchannels {
	    rnd = scop_random()	    
	    if (rnd <= transition_probability) { : the n-th channel changes state
		fraction = 0
		FROM j=0 TO CNNAF_NSTATES-1 {
		    if (i != j) {
			fraction = fraction + A[i*CNNAF_NSTATES+j] * dt
			if (rnd <= fraction) {
			    channels[i] = channels[i]-1
			    channels[j] = channels[j]+1
			    VERBATIM
			    break;
			    ENDVERBATIM
			}
		    }
		}
	    }
	}
    }
}

PROCEDURE fill_transition_matrix() {
    LOCAL am,bm,ah,bh
    am = alpham(v)
    bm = betam(v)
    ah = alphah(v)
    bh = betah(v)
    
    A[0] = -ah-3*am
    A[1] = 3*am
    A[2] = 0.0
    A[3] = 0.0
    A[4] = ah
    A[5] = 0.0
    A[6] = 0.0
    A[7] = 0.0
    
    A[8] = bm
    A[9] = -ah-2*am-bm
    A[10] = 2*am
    A[11] = 0.0
    A[12] = 0.0
    A[13] = ah
    A[14] = 0.0
    A[15] = 0.0
    
    A[16] = 0.0
    A[17] = 2*bm
    A[18] = -ah-am-2*bm
    A[19] = am
    A[20] = 0.0
    A[21] = 0.0
    A[22] = ah
    A[23] = 0.0
    
    A[24] = 0.0
    A[25] = 0.0
    A[26] = 3*bm
    A[27] = -ah-3*bm
    A[28] = 0.0
    A[29] = 0.0
    A[30] = 0.0
    A[31] = ah
    
    A[32] = bh
    A[33] = 0.0
    A[34] = 0.0
    A[35] = 0.0
    A[36] = -3*am-bh
    A[37] = 3*am
    A[38] = 0.0
    A[39] = 0.0
    
    A[40] = 0.0
    A[41] = bh
    A[42] = 0.0
    A[43] = 0.0
    A[44] = bm
    A[45] = -2*am-bh-bm
    A[46] = 2*am
    A[47] = 0.0
    
    A[48] = 0.0
    A[49] = 0.0
    A[50] = bh
    A[51] = 0.0
    A[52] = 0.0
    A[53] = 2*bm
    A[54] = -am-bh-2*bm
    A[55] = am
    
    A[56] = 0.0
    A[57] = 0.0
    A[58] = 0.0
    A[59] = bh
    A[60] = 0.0
    A[61] = 0.0
    A[62] = 3*bm
    A[63] = -bh-3*bm
}

FUNCTION vtrap(x,y) {  :Traps for 0 in denominator of rate eqns.
    if (fabs(x/y) < 1e-6) {
        vtrap = y*(1 - x/y/2)
    }else{
        vtrap = x/(exp(x/y) - 1)
    }
}
 

FUNCTION alpham(Vm (mV)) (/ms) {
    UNITSOFF
    alpham = .1 * vtrap(-(Vm+40),10)
    UNITSON
}


FUNCTION betam(Vm (mV)) (/ms) {
    UNITSOFF
    betam =  4 * exp(-(Vm+65)/18)
    UNITSON
}


FUNCTION alphah(Vm (mV)) (/ms) {
    UNITSOFF
    alphah = .07 * exp(-(Vm+65)/20)
    UNITSON
}


FUNCTION betah(Vm (mV)) (/ms) {
    UNITSOFF
    betah = 1 / (exp(-(Vm+35)/10) + 1)
    UNITSON
}

