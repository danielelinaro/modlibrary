#!/usr/bin/env python

from neuron import h
import nrnutils as nrn

#pyr = nrn.makeRS(L=61.4, diam=61.4, type='stochastic')
inh = nrn.makeLTS(L=89.2, diam=89.2)

dur = 2000
amp = {'pyr': 0.35, 'inh1': -0.05, 'inh2': 0.2}
delay = 1000
stim = {#'pyr': nrn.makeIclamp(pyr(0.5), dur, amp['pyr'], delay),
        'inh1': nrn.makeIclamp(inh(0.5), dur/2, amp['inh1'], delay/2),
        'inh2': nrn.makeIclamp(inh(0.5), dur/2, amp['inh2'], dur/2+0.5*delay)}
#rec = nrn.makeRecorders(pyr(0.5), {'vpyr': '_ref_v'})
rec = nrn.makeRecorders(inh(0.5), {'vinh': '_ref_v', 'ica': '_ref_ica'})#, rec)

h.celsius = 36
nrn.run(dur+2*delay, dt=0.005, V=-84)

import pylab as p

#p.figure()
#p.title('Excitatory')
#p.plot(rec['t'],rec['vpyr'],'k')
#p.xlabel('t (ms)')
#p.ylabel('V (mV)')

p.figure()
p.subplot(2,1,1)
p.title('Inhibitory')
p.plot(rec['t'],rec['vinh'],'k',label='Membrane potential')
p.xlabel('t (ms)')
p.ylabel('V (mV)')
#p.legend(loc='best')
p.subplot(2,1,2)
p.plot(rec['t'],rec['ica'],'k')
p.xlabel('t (ms)')
p.ylabel('I (nA)')
p.show()


