TITLE Cortical M current with channel noise
:
:   M-current, responsible for the adaptation of firing rate and the 
:   afterhyperpolarization (AHP) of cortical pyramidal cells
:
:   First-order model described by hodgkin-Hyxley like equations.
:   K+ current, activated by depolarization, noninactivating.
:
:   Model taken from Yamada, W.M., Koch, C. and Adams, P.R.  Multiple 
:   channels and calcium dynamics.  In: Methods in Neuronal Modeling, 
:   edited by C. Koch and I. Segev, MIT press, 1989, p 97-134.
:
:   See also: McCormick, D.A., Wang, Z. and Huguenard, J. Neurotransmitter 
:   control of neocortical neuronal activity and excitability. 
:   Cerebral Cortex 3: 387-398, 1993.
:
:   Written by Alain Destexhe, Laval University, 1995
:

INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
    SUFFIX im_cn
    USEION k READ ek WRITE ik
    RANGE gkbar, m_inf, tau_m
    GLOBAL taumax
    RANGE gk
    RANGE gamma, nchan, seed
}

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
    (pS) = (picosiemens)
    (um) = (micrometer)
}


PARAMETER {
    :v		(mV)
    celsius = 36    (degC)
    ek		(mV)
    gkbar	= 1e-6	(mho/cm2)
    taumax	= 1000	(ms)		: peak value of tau
    gamma  =  20       (pS)
    seed   =  5061983  (1)
}

STATE {
    m
    cn_z
}

ASSIGNED {
    ik	(mA/cm2)
    gk  (S/cm2)
    m_inf
    tau_m	(ms)
    tau_peak	(ms)
    tadj
    v         (mV)
    dt        (ms)
    area      (um2)
    nchan     (1)
    cn_mu     (1)
    cn_noise  (1)
    coeff     (1)
}

BREAKPOINT {
    SOLVE states
    gk = gkbar * (m + cn_z)
    if (gk < 0) {
	gk = 0
    }
    else if (gk > gkbar) {
	gk = gkbar
    }
    ik = gk * (v-ek)
}

PROCEDURE states() { 
    evaluate_fct(v)
    m = m + dt * (m_inf - m) / tau_m
    cn_z = cn_z * cn_mu + cn_noise
}

UNITSOFF
INITIAL {
    evaluate_fct(v)
    m = 0
    :
    :  The Q10 value is assumed to be 2.3
    :
    tadj = 2.3 ^ ((celsius-36)/10)
    tau_peak = taumax / tadj
    
    nchan = ceil(((1e-8)*area)*(gkbar)/((1e-12)*gamma))
    coeff = 1. / nchan
    :printf("im_cn>> NK = %.0f.\n", nchan)
    cn_z = 0
    set_seed(seed)
}

PROCEDURE evaluate_fct(v(mV)) {
    LOCAL ss
    m_inf = 1 / ( 1 + exptable(-(v+35)/10) )
    tau_m = tau_peak / ( 3.3 * exptable((v+35)/20) + exptable(-(v+35)/20) )
    
    ss = coeff * m_inf * (1-m_inf)
    cn_mu = exp(-dt/tau_m)
    cn_noise = sqrt(ss * (1-cn_mu*cn_mu)) * normrand(0,1)
}
UNITSON


FUNCTION exptable(x) { 
    TABLE FROM -25 TO 25 WITH 10000
    if ((x > -25) && (x < 25)) {
	exptable = exp(x)
    } else {
	exptable = 0.
    }
}
