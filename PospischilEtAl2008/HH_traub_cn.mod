TITLE Hippocampal HH channels with channel noise
:
: Fast Na+ and K+ currents responsible for action potentials
: Iterative equations
:
: Equations modified by Traub, for Hippocampal Pyramidal cells, in:
: Traub & Miles, Neuronal Networks of the Hippocampus, Cambridge, 1991
:
: range variable vtraub adjust threshold
:
: Written by Alain Destexhe, Salk Institute, Aug 1992
:
: Modified Oct 96 for compatibility with Windows: trap low values of arguments
:
: Modified Jun 2012 by Daniele Linaro

INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
    SUFFIX hh2_cn
    USEION na READ ena WRITE ina
    USEION k READ ek WRITE ik
    RANGE gnabar, gkbar, vtraub
    RANGE m_inf, h_inf, n_inf
    RANGE tau_m, tau_h, tau_n
    RANGE m_exp, h_exp, n_exp
    RANGE gna, gk
    RANGE gamma_na, gamma_k
    RANGE nchan_na, nchan_k
    RANGE seed, Y, Z
}

DEFINE HH2_CN_NA_STATES 7
DEFINE HH2_CN_K_STATES  4

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
    (pS) = (picosiemens)
    (um) = (micrometer)
}

PARAMETER {
    gnabar    = .003  (mho/cm2)
    gkbar     = .005  (mho/cm2)
    
    ena       = 50    (mV)
    ek        = -90   (mV)
    celsius   = 36    (degC)
    vtraub    = -63   (mV)
    
    gamma_na  =  20   (pS)
    gamma_k   =  20   (pS)
    
    seed   =  5061983  (1)
}

STATE {
    m (1)
    h (1)
    n (1)
    cn_y[HH2_CN_NA_STATES] (1)
    cn_z[HH2_CN_K_STATES] (1)
}

ASSIGNED {
    ina                (mA/cm2)
    ik                 (mA/cm2)
    il                 (mA/cm2)
    m_inf              (1)
    h_inf              (1)
    n_inf              (1)
    tau_m              (ms)
    tau_h              (ms)
    tau_n              (ms)
    m_exp              (1)
    h_exp              (1)
    n_exp              (1)
    tadj
    
    gna                (S/cm2)
    gk                 (S/cm2)
    v                  (mV)
    dt                 (ms)
    area               (um2)
    nchan_na           (1)
    nchan_k            (1)
    Y                  (1)
    Z                  (1)
    cn_tau_y[HH2_CN_NA_STATES]    (ms)
    cn_var_y[HH2_CN_NA_STATES]    (1)
    cn_noise_y[HH2_CN_NA_STATES]  (1)
    cn_mu_y[HH2_CN_NA_STATES]     (1)
    cn_tau_z[HH2_CN_K_STATES]    (ms)
    cn_var_z[HH2_CN_K_STATES]    (1)
    cn_noise_z[HH2_CN_K_STATES]  (1)
    cn_mu_z[HH2_CN_K_STATES]     (1)
}


BREAKPOINT {
    SOLVE states
    
    : sodium current
    Y = 0
    FROM i=0 TO HH2_CN_NA_STATES-1 {
	Y = Y + cn_y[i]
    }
    gna = gnabar * (m*m*m*h + Y)
    if (gna < 0) {
	gna = 0
    }
    else if (gna > gnabar) {
	gna = gnabar
    }
    ina = gna * (v - ena)
    
    : potassium current
    Z = 0
    FROM i=0 TO HH2_CN_K_STATES-1 {
	Z = Z + cn_z[i]
    }
    gk = gkbar * (n*n*n*n + Z)
    if (gk < 0) {
	gk = 0
    }
    else if (gk > gkbar) {
	gk = gkbar
    }
    ik  = gk * (v - ek)
}


PROCEDURE states() {    : exact when v held constant
    evaluate_fct(v)
    m = m + m_exp * (m_inf - m)
    h = h + h_exp * (h_inf - h)
    n = n + n_exp * (n_inf - n)
    FROM i=0 TO HH2_CN_NA_STATES-1 {
	cn_y[i] = cn_y[i] * cn_mu_y[i] + cn_noise_y[i]
    }
    FROM i=0 TO HH2_CN_K_STATES-1 {
	cn_z[i] = cn_z[i] * cn_mu_z[i] + cn_noise_z[i]
    }
    VERBATIM
    return 0;
    ENDVERBATIM
}

UNITSOFF
INITIAL {
    m = 0
    h = 0
    n = 0
    :
    :  Q10 was assumed to be 3 for both currents
    :
    : original measurements at room temperature?
    
    tadj = 3.0 ^ ((celsius-36)/ 10 )
    
    nchan_na = ceil(((1e-8)*area)*(gnabar)/((1e-12)*gamma_na))
    nchan_k = ceil(((1e-8)*area)*(gkbar)/((1e-12)*gamma_k))
    :printf("hh2_cn>> NNa = %.0f, NK = %.0f.\n", nchan_na, nchan_k)
    FROM i=0 TO HH2_CN_NA_STATES-1 {
	cn_y[i] = 0.0
    }
    FROM i=0 TO HH2_CN_K_STATES-1 {
	cn_z[i] = 0.0
    }
    set_seed(seed)
    
}

PROCEDURE evaluate_fct(v(mV)) {
    LOCAL a,b,v2,m_inf3,one_minus_m_inf,one_minus_h_inf,n_inf4,one_minus_n_inf
    
    v2 = v - vtraub : convert to traub convention
    
    : sodium channels
    :a = 0.32 * (13-v2) / ( Exp((13-v2)/4) - 1)
    a = 0.32 * vtrap(13-v2, 4)
    :b = 0.28 * (v2-40) / ( Exp((v2-40)/5) - 1)
    b = 0.28 * vtrap(v2-40, 5)
    tau_m = 1 / (a + b) / tadj
    m_inf = a / (a + b)
    
    a = 0.128 * Exp((17-v2)/18)
    b = 4 / ( 1 + Exp((40-v2)/5) )
    tau_h = 1 / (a + b) / tadj
    h_inf = a / (a + b)
    
    m_inf3 = m_inf*m_inf*m_inf
    one_minus_m_inf = 1.0 - m_inf
    one_minus_h_inf = 1.0 - h_inf
    
    cn_var_y[0] = 1.0 / nchan_na * m_inf3*m_inf3*h_inf * one_minus_h_inf
    cn_var_y[1] = 3.0 / nchan_na * m_inf3*m_inf*m_inf*h_inf*h_inf * one_minus_m_inf
    cn_var_y[2] = 3.0 / nchan_na * m_inf3*m_inf*h_inf*h_inf * one_minus_m_inf*one_minus_m_inf
    cn_var_y[3] = 1.0 / nchan_na * m_inf3*h_inf*h_inf * one_minus_m_inf*one_minus_m_inf*one_minus_m_inf
    cn_var_y[4] = 3.0 / nchan_na * m_inf3*m_inf*m_inf*h_inf * one_minus_m_inf*one_minus_h_inf
    cn_var_y[5] = 3.0 / nchan_na * m_inf3*m_inf*h_inf * one_minus_m_inf*one_minus_m_inf*one_minus_h_inf
    cn_var_y[6] = 1.0 / nchan_na * m_inf3*h_inf * one_minus_m_inf*one_minus_m_inf*one_minus_m_inf*one_minus_h_inf

    cn_tau_y[0] = tau_h
    cn_tau_y[1] = tau_m
    cn_tau_y[2] = tau_m/2
    cn_tau_y[3] = tau_m/3
    cn_tau_y[4] = tau_m*tau_h/(tau_m+tau_h)
    cn_tau_y[5] = tau_m*tau_h/(tau_m+2*tau_h)
    cn_tau_y[6] = tau_m*tau_h/(tau_m+3*tau_h)
    FROM i=0 TO HH2_CN_NA_STATES-1 {
        cn_mu_y[i] = Exp(-dt/cn_tau_y[i])
	cn_noise_y[i] = sqrt(cn_var_y[i] * (1-cn_mu_y[i]*cn_mu_y[i])) * normrand(0,1)
    }

    : potassium channels
    :a = 0.032 * (15-v2) / ( Exp((15-v2)/5) - 1)
    a = 0.032 * vtrap(15-v2, 5)
    b = 0.5 * Exp((10-v2)/40)
    tau_n = 1 / (a + b) / tadj
    n_inf = a / (a + b)

    n_inf4 = n_inf^4
    one_minus_n_inf = 1.0 - n_inf
    cn_var_z[0] = 4.0/nchan_k * n_inf4*n_inf*n_inf*n_inf * one_minus_n_inf
    cn_var_z[1] = 6.0/nchan_k * n_inf4*n_inf*n_inf * one_minus_n_inf*one_minus_n_inf
    cn_var_z[2] = 4.0/nchan_k * n_inf4*n_inf * one_minus_n_inf*one_minus_n_inf*one_minus_n_inf
    cn_var_z[3] = 1.0/nchan_k * n_inf4 * one_minus_n_inf*one_minus_n_inf*one_minus_n_inf*one_minus_n_inf
    FROM i=0 TO HH2_CN_K_STATES-1 {
	cn_tau_z[i] = tau_n / (i+1)
	cn_mu_z[i] = Exp(-dt/cn_tau_z[i])
	cn_noise_z[i] = sqrt(cn_var_z[i] * (1-cn_mu_z[i]*cn_mu_z[i])) * normrand(0,1)
    }
    
    m_exp = 1 - Exp(-dt/tau_m)
    h_exp = 1 - Exp(-dt/tau_h)
    n_exp = 1 - Exp(-dt/tau_n)
}

FUNCTION vtrap(x,y) {
    if (fabs(x/y) < 1e-6) {
	vtrap = y*(1 - x/y/2)
    }else{
	vtrap = x/(Exp(x/y)-1)
    }
}

FUNCTION Exp(x) {
    if (x < -100) {
	Exp = 0
    }else{
	Exp = exp(x)
    }
} 
