COMMENT
Current clamp that generates a Ornstein-Uhlenbeck current

Author: Daniele Linaro
Date: August 21, 2009
ENDCOMMENT

NEURON {
    POINT_PROCESS IClampOU
    RANGE delay, dur, amp
    RANGE sigma, tau
    RANGE iaux, D, mu, sigmax
    RANGE seed
    ELECTRODE_CURRENT i
    THREADSAFE
}

VERBATIM
#ifndef DL_RANDOM
#define DL_RANDOM

typedef unsigned long long ullong;
typedef struct {
        ullong j, u, w, z;
        double storedval;
} norm_random;

#endif

norm_random iclampou_rand;
#define j iclampou_rand.j
#define u iclampou_rand.u
#define w iclampou_rand.w
#define z iclampou_rand.z
#define storedval iclampou_rand.storedval

ENDVERBATIM

UNITS {
    (nA) = (nanoamp)
}

PARAMETER {
    delay          (ms)
    dur            (ms)
    amp            (nA)
    sigma = 0      (nA)
    tau  = 20      (ms)
    seed = 5061983 (1)
}

FUNCTION int64() {
    VERBATIM
    {
	u = u * 2862933555777941757LL + 7046029254386353087LL; 
	w ^= w >> 17;
	w ^= w << 31;
	w ^= w >> 8; 
	z = 4294957665U*(z & 0xffffffff) + (z >> 32); 
	ullong x = u ^ (u << 21); x ^= x >> 35; x ^= x << 4; 
	return (x + w) ^ z;
    }
    ENDVERBATIM
}

ASSIGNED {
    i      (nA)
    iaux   (nA)
    dt     (ms)
    D      (nA2/ms)
    mu     (1)
    sigmax (nA)
}

INITIAL {
    i = 0
    iaux = amp
    D = 2*sigma*sigma/tau     : diffusion constant
    mu = exp(-dt/tau)         : decay constant
    sigmax = sqrt(D*tau/2*(1-mu*mu))  : a coefficient
    rand_init(seed)
}

PROCEDURE rand_init(seed) {
    VERBATIM
    j = (ullong) seed;
    w = 4101842887655102017LL;
    z = 1;
    u = j ^ w; int64();
    w = u; int64();
    z = w; int64();
    storedval = 0.0;
    ENDVERBATIM
}

FUNCTION doub() {
    VERBATIM
    return 5.42101086242752217E-20 * int64();
    ENDVERBATIM
}

FUNCTION normdeviate() {
    VERBATIM
    {
	/* returns a random normal number with 0 mean and unitary variance */
	double v1,v2,rsq,fac; 
	if (storedval == 0.) {
	    do { 
		v1 = 2.0*doub()-1.0;
		v2 = 2.0*doub()-1.0; 
		rsq=v1*v1+v2*v2;
	    } while (rsq >= 1.0 || rsq == 0.0); 
	    fac = sqrt(-2.0*log(rsq)/rsq);
	    storedval = v1*fac; 
	    return v2*fac;
	} else {
	    fac = storedval; 
	    storedval = 0.; 
	    return fac;
	} 
    }
    ENDVERBATIM
}

BREAKPOINT {
    LOCAL iaux
    at_time(delay)
    at_time(delay+dur)
    if (t>=delay && t<=delay+dur) {
	iaux = i - amp
	iaux = iaux*mu + sigmax*normdeviate()
	i = amp + iaux
    } else {
	i = 0
    }
}

