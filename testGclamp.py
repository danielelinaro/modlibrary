#!/usr/bin/env python

from neuron import h
import nrnutils as nrn
import numpy as np
import pylab as p

L = 60
diam = 60
mu = 2
sigma = 6
tau = 50
e = 0
dt = 0.005
dur = 3000
delay = 200
soma = nrn.makeRS(60,60)
stim,vec = nrn.makeNoisyGclamp(soma(0.5), dur, dt, mu, sigma, tau, e, delay)
rec = nrn.makeRecorders(soma(0.5), {'v':'_ref_v'})
rec = nrn.makeRecorders(stim, {'i':'_ref_i','g':'_ref_g'}, rec)

h.celsius = 36
nrn.run(dur+delay+500,dt)

p.figure()
p.subplot(1,2,1)
p.plot(rec['t'],rec['v'],'k',label='Vm (mV)')
p.plot(rec['t'],rec['g'],'r',label='G (nS)')
p.xlabel('Time (ms)')
p.legend(loc='best')
p.subplot(1,2,2)
p.plot(rec['t'],rec['i'],'k')
p.xlabel('Time (ms)')
p.ylabel('Current (nA)')
p.show()

