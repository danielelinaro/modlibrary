COMMENT
 Authors: Vincent Delattre & Michele Giugliano

 Simple implementation of the (stochastic) conductance-based model introduced by Wang & Buzsaki (1996).
 Based on na_stochastic.mod by Stefan Hallermann and on the work of Kole et al. (2006).

 In the initiation the number of channels (N) is calculated for each segment and stored as a
 range variable. For each dt the procedure noise() is evaluated once. In noise() each channel
 in each state has the chance to move to one of its neighbouring states with the appropriate
 probability. After this "update" of the number of channels in each state the resulting current
 through the open channels (i) is calculated depending on the local driving force in the
 segment. The eight-state reaction model was adopted from Hille (1978) and the resulting
 kinetics are identical to Na-kinetics of Mainen and Sejnowski (1996). 

 Caution 1: Because of the fast kinetics of Na-channels the iteration time dt should be 1 micro second.
 Caution 2: You should wait at the beginning of the simulation until the Na channels have reached 
            steady state (i.e. distributed in a voltage dependent manner over the eight states of
            the reaction scheme). 
	    
 Kole MH, Hallermann S, Stuart GJ (2006) Single Ih channels in pyramidal neuron dendrites: 
	    properties, distribution, and impact on action potential output. J Neurosci 26:1677-87
 Wang XJ, Buzsaki G (1996) Gamma oscillation by synaptic inhibition in a hippocampal interneuronal network model. J Neurosci 16:6402-13
ENDCOMMENT


COMMENT
!!!
Reviewed version by Daniele Linaro (daniele.linaro@unige.it) - August 2009
!!!
ENDCOMMENT


INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}


NEURON {
    SUFFIX WBmicro
    USEION na READ ena WRITE ina
    USEION k READ ek WRITE ik
    RANGE phi
    RANGE gnabar, gkbar, gna, gk
    RANGE gamma_na, gamma_k
    RANGE m0h0,m1h0,m2h0,m3h0,m0h1,m1h1,m2h1,m3h1
    RANGE n0, n1, n2, n3, n4
    RANGE Nna, Nk
    RANGE nrand, ntrans
    THREADSAFE : assigned GLOBALs will be per thread
} : end NEURON


UNITS {
    (mA) = (milliamp)
    (mV) = (millivolt)
    (S)  = (siemens)
    (pS) = (picosiemens)
    (um) = (micron)
} : end UNITS


PARAMETER {
    gnabar   = 0.035  (S/cm2)     : maximum sodium conductance
    gkbar    = 0.015  (S/cm2)     : maximum potassium conductance

    gamma_na = 10  (pS)		: single channel sodium conductance
    gamma_k  = 10  (pS)		: single channel potassium conductance
    seed = 5061983              : always use the same seed (my birth date) so that results can be reproduced
    phi      = 5                : (dimensionless) parameter of the Wang-Buzsaki model
    
    ena  (mV)
    ek   (mV)
    celsius  (degC)
    v  (mV)
} : end PARAMETER


STATE {
    m h
} : end STATE


ASSIGNED {
    ina   (mA/cm2)
    ik    (mA/cm2)
    gna   (S/cm2)
    gk    (S/cm2)
    
    dt    (ms)
    area  (um2)
    
    Nna			 	: total number of sodium channels
    Nk			 	: total number of potassium channels

    m0h0			: inactivated state (sodium channels)
    m1h0			: inactivated state (sodium channels)
    m2h0			: inactivated state (sodium channels)
    m3h0			: inactivated state (sodium channels)
    m0h1			: closed state (sodium channels)
    m1h1			: closed state (sodium channels)
    m2h1			: closed state (sodium channels)
    m3h1			: open state (sodium channels)

    n0				: closed state (potassium channels)
    n1				: closed state (potassium channels)
    n2				: closed state (potassium channels)
    n3				: closed state (potassium channels)
    n4				: open state (potassium channels)
    
    nrand  : number of generated random numbers
    ntrans : number of channels' transitions
    
} : end ASSIGNED



INITIAL {
    m = 0
    h = 0

    Nna = ceil(((1e-8)*area)*(gnabar)/((1e-12)*gamma_na))   : area in um2 -> 1e-8*area in cm2; gnabar in S/cm2; gamma_na in pS -> 1e-12*gamma_na in S
    Nk = ceil(((1e-8)*area)*(gkbar)/((1e-12)*gamma_k))   : area in um2 -> 1e-8*area in cm2; gkbar in S/cm2; gamma_k in pS -> 1e-12*gamma_k in S
    
    m0h0 = 0
    m1h0 = 0
    m2h0 = 0
    m3h0 = 0
    m0h1 = Nna		: therefore you should wait at the beginning of the simulation until the Na channels have reached steady state.
    m1h1 = 0
    m2h1 = 0
    m3h1 = 0

    n0   = Nk		: therefore you should wait at the beginning of the simulation until the K channels have reached steady state.
    n1   = 0
    n2   = 0
    n3   = 0
    n4   = 0
    
    nrand = 0
    ntrans = 0
    
    set_seed(seed)
    
} : end INITIAL


BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = m3h1*((1e-12)*gamma_na)/((1e-8)*area)
    gk = n4*((1e-12)*gamma_k)/((1e-8)*area)
    ina = gna * (v-ena)
    ik = gk * (v-ek)
} : end BREAKPOINT


UNITSOFF

DERIVATIVE states {   
        m' = m
        h' = h
	noise()
} : end DERIVATIVE

FUNCTION scop_random_wrapper() (1) {
    nrand = nrand+1
    scop_random_wrapper = scop_random()
}

PROCEDURE increment_ntrans() {
    ntrans = ntrans+1
}

PROCEDURE noise() {
    LOCAL p,am,bm,ah,bh,an,bn,m0h0_merk,m1h0_merk,m2h0_merk,m3h0_merk,m0h1_merk,m1h1_merk,m2h1_merk,m3h1_merk,n0_merk,n1_merk,n2_merk,n3_merk,n4_merk,rnd,den
    
    : alpha_m and beta_m
    if (v == -35) { 
	am = 1 
    }
    else {
        am = 0.1 * (v+35.) / ( 1. - exp(-0.1*(v+35.)) )
    }
    bm = 4. * exp(-(v+60.)/18.)    
    
    : alpha_h and beta_h
    ah = phi * 0.07 * exp(-0.05*(v+58.))
    bh = phi * 1.0 / ( 1. + exp(-0.1*(v+28)) )
    
    : alpha_n and beta_n
    if (v == -34) {
	an = phi * 0.1 
    }
    else { 
        an = phi * 0.01 * (v+34.) / ( 1. - exp(-0.1*(v+34.)) )
    }
    bn = phi * 0.125 * exp(-0.0125*(v+44))

    m0h0_merk = m0h0
    m1h0_merk = m1h0
    m2h0_merk = m2h0
    m3h0_merk = m3h0
    m0h1_merk = m0h1
    m1h1_merk = m1h1
    m2h1_merk = m2h1
    m3h1_merk = m3h1

    n0_merk = n0
    n1_merk = n1
    n2_merk = n2
    n3_merk = n3
    n4_merk = n4
    
    : ------------- h0 -------------
    
    :m0h0
    den = ah+3*am
    p = 1-exp(-dt*den)
    FROM ii=1 TO m0h0_merk {
	:scop_random gives random number uniform between 0 and 1
	if (scop_random_wrapper() <= p)	{  :probability that a channel in the state m0h0 goes to state m0h1 or m1h0
	    increment_ntrans()
	    if (scop_random_wrapper() <= ah/den) {  :probability that this channel goes to state m0h1 (via rate ah)
		m0h0=m0h0-1
		m0h1=m0h1+1
	    }
	    else {  :otherwise this channel goes to m1h0 (via rate 3*am)
		m0h0=m0h0-1
		m1h0=m1h0+1
	    }
	}
    }
    
    :m1h0
    den = ah+2*am+bm
    p = 1-exp(-dt*den)
    FROM ii=1 TO m1h0_merk {
	if (scop_random_wrapper() <= p) {
	    increment_ntrans()
	    rnd = scop_random_wrapper()
	    if (rnd <= ah/den) {	
		m1h0=m1h0-1
		m1h1=m1h1+1
	    }
	    else if (rnd <= (ah+2*am)/den) {
		m1h0=m1h0-1
		m2h0=m2h0+1
	    }
	    else {
		m1h0=m1h0-1
		m0h0=m0h0+1
	    }
	}
    }
    
    :m2h0
    den = ah+am+2*bm
    p = 1-exp(-dt*den) 
    FROM ii=1 TO m2h0_merk {
	if (scop_random_wrapper() <= p){
	    increment_ntrans()
	    rnd = scop_random_wrapper()
	    if (rnd <= ah/den)	{
		m2h0=m2h0-1
		m2h1=m2h1+1
	    }
	    else if (rnd <= (ah+am)/den) {
		m2h0=m2h0-1
		m3h0=m3h0+1
	    }
	    else {
		m2h0=m2h0-1
		m1h0=m1h0+1
	    }
	}
    }
    
    :m3h0
    den = ah+3*bm
    p = 1-exp(-dt*den)
    FROM ii=1 TO m3h0_merk {
	if (scop_random_wrapper() <= p) {
	    increment_ntrans()
	    rnd = scop_random_wrapper()
	    if (rnd <= ah/den) {	
		m3h0=m3h0-1
		m3h1=m3h1+1
	    }
	    else {	
		m3h0=m3h0-1
		m2h0=m2h0+1
	    }
	}
    }
    
    
    : ------------- h1 -------------
    
    :m0h1
    den = bh+3*am
    p=1-exp(-dt*den) 
    FROM ii=1 TO m0h1_merk {
	if (scop_random_wrapper() <= p) {
	    increment_ntrans()
	    rnd = scop_random_wrapper()
	    if (rnd <= bh/den)	{	
		m0h1=m0h1-1
		m0h0=m0h0+1
	    }
	    else {
		m0h1=m0h1-1
		m1h1=m1h1+1
	    }
	}
    }
    
    :m1h1
    den = bh+2*am+bm
    p = 1-exp(-dt*den) 
    FROM ii=1 TO m1h1_merk {
	if (scop_random_wrapper() <= p) {
	    increment_ntrans()
	    rnd = scop_random_wrapper()
	    if (rnd <= bh/den)	{	
		m1h1=m1h1-1
		m1h0=m1h0+1
	    }
	    else if (rnd <= (bh+2*am)/den) {	
		m1h1=m1h1-1
		m2h1=m2h1+1
	    }
	    else {
		m1h1=m1h1-1
		m0h1=m0h1+1
	    }
	}
    }
    
    :m2h1
    den = bh+am+2*bm
    p = 1-exp(-dt*den) 
    FROM ii=1 TO m2h1_merk {
	if (scop_random_wrapper()<= p){
	    increment_ntrans()
	    rnd = scop_random_wrapper()
	    if (rnd <= bh/den)	{	
		m2h1=m2h1-1
		m2h0=m2h0+1
	    }
	    else if (rnd <= (bh+am)/den) {	
		m2h1=m2h1-1
		m3h1=m3h1+1
	    }
	    else {
		m2h1=m2h1-1
		m1h1=m1h1+1
	    }
	}
    }
    
    :m3h1
    den = bh+3*bm
    p = 1-exp(-dt*den) 
    FROM ii=1 TO m3h1_merk {
	if (scop_random_wrapper()<= p) {
	    increment_ntrans()
	    rnd = scop_random_wrapper()
	    if (rnd <= bh/den)	{	
		m3h1=m3h1-1
		m3h0=m3h0+1
	    }	
	    else {
		m3h1=m3h1-1
		m2h1=m2h1+1
	    }
	}
    }
    
    
    : ------------- n -------------
    
    :n0
    den = 4*an
    p = 1-exp(-dt*den) 
    FROM ii=1 TO n0_merk {
	if (scop_random_wrapper() <= p) {
	    increment_ntrans()
	    n0=n0-1
	    n1=n1+1	
	}
    }
    
    
    :n1
    den = 3*an+bn
    p = 1-exp(-dt*den) 
    FROM ii=1 TO n1_merk {
	if (scop_random_wrapper() <= p) {
	    increment_ntrans()
	    if (scop_random_wrapper() <= 3*an/den) {	
		n1=n1-1
		n2=n2+1
	    }
	    else {
		n1=n1-1
		n0=n0+1
	    }
	}
    }
    
    
    :n2
    den = 2*an+2*bn
    p = 1-exp(-dt*den)
    FROM ii=1 TO n2_merk {
	if (scop_random_wrapper() <= p) {
	    increment_ntrans()
	    if (scop_random_wrapper() <= 2*an/den) {
		n2=n2-1
		n3=n3+1
	    }
	    else {
		n2=n2-1
		n1=n1+1
	    }
	}
    }
    
    
    :n3
    den = an+3*bn
    p = 1-exp(-dt*den)
    FROM ii=1 TO n3_merk {
	if (scop_random_wrapper() <= p) {
	    increment_ntrans()
	    if (scop_random_wrapper() <= an/den) {
		n3=n3-1
		n4=n4+1
	    }
	    else {
		n3=n3-1
		n2=n2+1
	    }
	}
    }
    
    
    :n4
    den = 4*bn
    p = 1-exp(-dt*den)
    FROM ii=1 TO n4_merk {
	if (scop_random_wrapper() <= p) {
	    increment_ntrans()
	    n4=n4-1
	    n3=n3+1	
	}
    }
}

UNITSON

