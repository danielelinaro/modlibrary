COMMENT
Noisy square wave current

Author: Daniele Linaro
Date: January 21, 2010
ENDCOMMENT

NEURON {
    POINT_PROCESS SquareWaveOU
    RANGE i, iaux
    RANGE delay, dur
    RANGE a, f, T, duty_cycle
    RANGE mu, sigma, tau
    : k and sigmax are needed only when the exact method of solution is used
    RANGE sigmax, k
    RANGE seed, pi
    ELECTRODE_CURRENT i
    THREADSAFE
}

VERBATIM
#define SWOU _swou
typedef unsigned long long ullong;
ullong jSWOU, uSWOU, wSWOU, zSWOU;
double storedvalSWOU;
ENDVERBATIM

UNITS {
    (nA) = (nanoamp)
}

PARAMETER {
    delay= 0  (ms)
    dur       (ms)
    a    = 1  (nA)
    f    = 10 (/s)
    duty_cycle = 0.5 (1)
    mu   = 0  (nA)
    sigma= 0  (nA)
    tau  = 10 (ms)
    seed = 5061983
    pi = 3.1415926535897931
}

FUNCTION int64() {
    VERBATIM
    {
	uSWOU = uSWOU * 2862933555777941757LL + 7046029254386353087LL; 
	wSWOU ^= wSWOU >> 17;
	wSWOU ^= wSWOU << 31;
	wSWOU ^= wSWOU >> 8; 
	zSWOU = 4294957665U*(zSWOU & 0xffffffff) + (zSWOU >> 32); 
	ullong x = uSWOU ^ (uSWOU << 21); x ^= x >> 35; x ^= x << 4; 
	return (x + wSWOU) ^ zSWOU;
    }
    ENDVERBATIM
}

ASSIGNED {
    i    (nA)
    iaux (nA)
    T    (s)
    dt   (ms)
    : k and sigmax are needed only when the exact method of solution is used
    sigmax (nA)
    k      (1)
}

INITIAL {
    i = 0
    iaux = 0
    T = 1.0 / f
    :VERBATIM
    :fprintf(stderr, "T = %e\n", T);
    :ENDVERBATIM
    : k and sigmax are needed only when the exact method of solution is used
    k = exp(-dt/tau)
    sigmax = sqrt(sigma*sigma*(1-k*k))
    :sigmax = sqrt((sigma*sigma*tau/2)*(1-k*k))
    rand_init(seed)
}

PROCEDURE rand_init(seed) {
    VERBATIM
    jSWOU = (ullong) seed;
    wSWOU = 4101842887655102017LL;
    zSWOU = 1;
    uSWOU = jSWOU ^ wSWOU; int64();
    wSWOU = uSWOU; int64();
    zSWOU = wSWOU; int64();
    storedvalSWOU = 0.0;
    ENDVERBATIM
}

FUNCTION doub() {
    VERBATIM
    return 5.42101086242752217E-20 * int64();
    ENDVERBATIM
}

FUNCTION normdeviate() {
    VERBATIM
    {
	/* returns a random normal number with 0 mean and unitary variance */
	double v1,v2,rsq,fac; 
	if (storedvalSWOU == 0.) {
	    do {
		v1 = 2.0*doub()-1.0;
		v2 = 2.0*doub()-1.0; 
		rsq=v1*v1+v2*v2;
	    } while (rsq >= 1.0 || rsq == 0.0); 
	    fac = sqrt(-2.0*log(rsq)/rsq);
	    storedvalSWOU = v1*fac; 
	    return v2*fac;
	} else {
	    fac = storedvalSWOU; 
	    storedvalSWOU = 0.; 
	    return fac;
	} 
    }
    ENDVERBATIM
}

BREAKPOINT {
    LOCAL nperiods, time_in_period
    at_time(delay)
    at_time(delay+dur)
    if (t<delay+dur && t>delay) {
	: Exact solution
	iaux = iaux*k + sigmax * normdeviate()
	: Euler
	:iaux = (1-dt/tau)*iaux + sqrt(2/tau*dt) * sigma * normdeviate()
	i = mu + iaux
	nperiods = floor((t/1000)/T)
	time_in_period = t/1000 - nperiods*T
	if(time_in_period < T*duty_cycle) {
	    i = i + a
	}
	:VERBATIM
	:fprintf(stderr, "np = %d\ttp = %e\n", nperiods, time_in_period);
	:getchar();
	:ENDVERBATIM
    }
    else {
	i = 0
    }
}

