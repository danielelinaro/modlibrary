#!/usr/bin/env python

from neuron import h
import nrnutils as nrn
import numpy as np
import pylab as p

soma = h.Section()
soma.L = 50
soma.diam = 50
soma.insert('hh')

stim = h.IClampOU(soma(0.5))
stim.amp = 0.5
stim.sigma = 0.05
stim.tau = 20
stim.dur = 1000
stim.delay = 0

rec = nrn.makeRecorders(soma(0.5), {'v': '_ref_v'})
rec = nrn.makeRecorders(stim, {'i': '_ref_i'}, rec)

h.load_file('stdrun.hoc')
h.tstop = stim.dur
h.dt = 0.001
h.run()

t = np.asarray(rec['t'])
i = np.asarray(rec['i'])
idx = np.nonzero(t > 100)
print('Mean = %g\nStandard deviation = %g\n' % (np.mean(i[idx]),np.std(i[idx])))

p.figure()
p.plot(rec['t'],rec['v'],'k')
p.figure()
p.plot(rec['t'],rec['i'],'k')
p.show()
