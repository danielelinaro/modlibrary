#!/usr/bin/env python

from neuron import h
import numpy as np
import pylab as p

def run(type):
    soma = h.Section()
    cvode = h.CVode()
    if type == 'original':
        soma.insert('na3')
        soma.insert('kd3')
    else:
        soma.insert('na3bis')
        soma.insert('kd3bis')
    rec = {}
    for lbl in 't','v':
        rec[lbl] = h.Vector()
    rec['t'].record(h._ref_t)
    rec['v'].record(soma(0.5)._ref_v)
    h.load_file('stdrun.hoc')
    if type == 'original':
        cvode.active(0)
    else:
        cvode.active(1)
        cvode.rtol(1e-12)
        cvode.atol(1e-9)
    h.finitialize()
    h.tstop = 5000
    h.run()
    p.plot(rec['t'],rec['v'],label=type)

def runHH(withCVode=False):
    soma = h.Section()
    cvode = h.CVode()
    soma.insert('hh')
    soma.L = 50
    soma.diam = 50
    stim = h.IClamp(soma(0.5))
    stim.amp = 0.5
    stim.delay = 100
    stim.dur = 900
    apc = h.APCount(soma(0.5))
    rec = {}
    for lbl in 't','v':
        rec[lbl] = h.Vector()
    rec['t'].record(h._ref_t)
    rec['v'].record(soma(0.5)._ref_v)
    spks = h.Vector()
    apc.record(spks)
    h.load_file('stdrun.hoc')
    if withCVode:
        cvode.active(1)
        cvode.rtol(1e-12)
        cvode.atol(1e-9)
    else:
        cvode.active(0)
    h.finitialize()
    h.tstop = 1000
    h.run()
    spiketimes = np.asarray(spks)
    isi = np.diff(spiketimes)
    p.subplot(2,1,1)
    p.plot(rec['t'],rec['v'],label='with CVode: %d'%withCVode)
    p.subplot(2,1,2)
    p.plot(isi,'o')

p.figure()
run('original')
run('modified')
#runHH(True)
#runHH(False)
#p.subplot(2,1,1)
p.ylabel('Membrane voltage (mV)')
p.legend(loc='best')
#p.subplot(2,1,2)
p.xlabel('Time (ms)')
#p.ylabel('ISI (ms)')
p.show()
