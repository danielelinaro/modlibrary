#!/usr/bin/env python

from neuron import h
import numpy as np

post = h.Section()
post.insert('pas')
post.L = 5
post.diam = 6

events = h.Vector([100,200])
pre = h.BiGammaStim()
pre.order = 2
pre.toggle(events)

syn = h.ExpSyn(post(0.5))

rec = {}
for lbl in 't','v':
    rec[lbl] = h.Vector()
rec['t'].record(h._ref_t)
rec['v'].record(post(0.5)._ref_v)

spikes = h.Vector()
nc = h.NetCon(pre, syn)
nc.record(spikes)
nc.weight[0] = 0.001
h.load_file('stdrun.hoc')
h.tstop = 300
h.run()

import pylab as p
p.plot(rec['t'],rec['v'],'k')
p.show()

print np.asarray(spikes)
