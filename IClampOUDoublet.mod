COMMENT
Current clamp that generates a Ornstein-Uhlenbec random current

Author: Daniele Linaro
Date: August 21, 2009
ENDCOMMENT

NEURON {
    POINT_PROCESS IClampOU
    RANGE del, dur, i, iaux
    RANGE mu, sigma, tau
    : k and sigmax are needed only when the exact method of solution is used
    RANGE sigmax, k
    RANGE seed
    ELECTRODE_CURRENT i
    THREADSAFE
}

VERBATIM
typedef unsigned long long ullong;
ullong j, u, w, z;
double storedval;
ENDVERBATIM

UNITS {
    (nA) = (nanoamp)
}

PARAMETER {
    del  (ms)
    dur  (ms)
    mu   (nA)
    sigma (nA)
    tau  = 10 (ms)
    seed = 5061983
}

FUNCTION int64() {
    VERBATIM
    {
	u = u * 2862933555777941757LL + 7046029254386353087LL; 
	w ^= w >> 17;
	w ^= w << 31;
	w ^= w >> 8; 
	z = 4294957665U*(z & 0xffffffff) + (z >> 32); 
	ullong x = u ^ (u << 21); x ^= x >> 35; x ^= x << 4; 
	return (x + w) ^ z;
    }
    ENDVERBATIM
}

ASSIGNED {
    i    (nA)
    iaux (nA)
    dt   (ms)
    : k and sigmax are needed only when the exact method of solution is used
    sigmax (nA)
    k
}

INITIAL {
    i = 0
    iaux = 0
    : k and sigmax are needed only when the exact method of solution is used
    k = exp(-dt/tau)
    sigmax = sqrt(sigma*sigma*(1-k*k))
    :sigmax = sqrt((sigma*sigma*tau/2)*(1-k*k))
    rand_init(seed)
}

PROCEDURE rand_init(seed) {
    VERBATIM
    j = (ullong) seed;
    w = 4101842887655102017LL;
    z = 1;
    u = j ^ w; int64();
    w = u; int64();
    z = w; int64();
    storedval = 0.0;
    ENDVERBATIM
}

FUNCTION doub() {
    VERBATIM
    return 5.42101086242752217E-20 * int64();
    ENDVERBATIM
}

FUNCTION normdeviate() {
    VERBATIM
    {
	/* returns a random normal number with 0 mean and unitary variance */
	double v1,v2,rsq,fac; 
	if (storedval == 0.) {
	    do { 
		v1 = 2.0*doub()-1.0;
		v2 = 2.0*doub()-1.0; 
		rsq=v1*v1+v2*v2;
	    } while (rsq >= 1.0 || rsq == 0.0); 
	    fac = sqrt(-2.0*log(rsq)/rsq);
	    storedval = v1*fac; 
	    return v2*fac;
	} else {
	    fac = storedval; 
	    storedval = 0.; 
	    return fac;
	} 
    }
    ENDVERBATIM
}

BREAKPOINT {
    at_time(del)
    at_time(del+dur)
    if (t<del+dur && t>del) {
	: Exact solution
	iaux = iaux*k + sigmax * normdeviate()
	i = mu + iaux
	: Euler
	:iaux = (1-dt/tau)*iaux + sqrt(2/tau*dt) * sigma * normdeviate()
	:i = mu + iaux
    } else {
	i = 0
    }
}

