COMMENT

This is the original Hodgkin-Huxley treatment for the set of sodium,  potassium, and
leakage channels found in the squid giant axon membrane, incorporating the slow-cumulative
sodium inactivation, as reported by Miles et al., 2005.

PARAMETERS

 gnabar = .12   (S/cm2)    : maximal conductance for the sodium current.
 gkbar  = .036  (S/cm2)    : maximal conductance for the potassium current.
 gl     = .0003 (S/cm2)    : leak-current conductance.
 el     = -54.  (mV)       : reversal potential of the leak-current.

REFERENCES

Hodgkin, A.L., Huxley, A.F. (1952). A quantitative description of membrane current and its application conduction and excitation in nerve" J.Physiol. (Lond.) 117:500-544.
Miles, G.B., Dai, Y., and Brownstone, R.M. (2005). Mechanisms underlying the early phase of spike frequency adaptation in mouse spinal motoneurones. J Physiol 566.2 (2005) pp 519-532.
Arsiero, M., Luescher, H.-R., Lundstrom, B.N., and Giugliano, M. (2007). The Impact of Input Fluctuations on the Frequency-Current Relationships of Layer 5 Pyramidal Neurons in the Rat Medial Prefrontal Cortex. sumbitted.
  
AUTHORS

Willem Wybo
Modified from Michele Giugliano & Brian N. Lundstrom, Okinawa, June 5th 2006, and Lausanne Jan 5th 2007.
Modified from the original "hh.hoc" (SW Jaslove  6 March, 1992), provided with each standard distribution of NEURON.

ENDCOMMENT
 
 
UNITS {
        (mA) = (milliamp)
        (mV) = (millivolt)
         (S) = (siemens)
}
 
? interface
NEURON {
        SUFFIX csgilb
        USEION na READ ena WRITE ina
        USEION k READ ek WRITE ik
        NONSPECIFIC_CURRENT il
        RANGE gnabar, gkbar, gabar, gl, el, T0, gna, gk, active
        RANGE an, bn, am, bm, ah, bh, as, bs :, es, fs
}
 	
PARAMETER {
        gnabar = .12   (S/cm2)    <0,1e9>
        gkbar  = .02   (S/cm2)    <0,1e9>
        :ena	   	= 55 	(mV)
        :ek	   = -72	(mV)
        gl     = .0003 (S/cm2)    <0,1e9>
        el     = -17  (mV)
	T0     = 37 (degC)
}
 
STATE {
        ck4 ck3 ck2 ck1 ok :sk 					:potassium channel
        cna7 cna6 cna5 cna4 cna3 cna2 cna1 ona :sodium channel
        s1 s2 s3 s4 s5 s6 s7 s8 s9 s_10 s11 s12 s13 s14 s15
        s16 s17 s18 s19 s_20 s21 s22 s23 s24 s25 s26 s27 s28
        s29 s_30 s31 s32 s33 s34 s35 s36 s37 s38 s39 s_40 s41
        s42 s43 s44 s45 s46 s47 s48 s49 s_50 s51 s52 s53 s54 
        s55 s56 s57 s58 s59 s_60 s61 s62 s63 s64 s65 s66 s67
        s68 s69 s_70 s71 s72 s73 s74 s75 s76 s77 s78 s79 s_80
        s81 s82 s83 s84 s85 s86 s87 s88 s89 s_90 s91 s92 s93 
        s94 s95 s96 s97 s98 s99 :s_100
}

ASSIGNED {
        v       (mV)
        celsius (degC)
        ena     (mV)
        ek      (mV)

        gna (S/cm2)
        gk  (S/cm2)
        ina (mA/cm2)
        ik  (mA/cm2)
        il  (mA/cm2)
        
        active
        
        an (/ms)
        bn (/ms)
        am (/ms)
        bm (/ms)
        ah (/ms)
        bh (/ms)
        as (/ms)
        bs (/ms)
        es (/ms)
        fs (/ms)
}
 
:LOCAL mexp, hexp, sexp, nexp, aexp, bexp        
 
? currents
BREAKPOINT {
        SOLVE kin METHOD sparse
        gna = gnabar*ona
        ina = gna*(v - ena)
        gk = gkbar*ok
        ik = gk*(v - ek)
        il = gl*(v - el)
        active = cna7 + cna6 + cna5 + cna4 + cna3 + cna2 + cna1 + ona
}
 
KINETIC kin {
	rates(v)
	~ ck4 <-> ck3 (4*an, bn)
	~ ck3 <-> ck2 (3*an, 2*bn)
	~ ck2 <-> ck1 (2*an, 3*bn)
	~ ck1 <-> ok (an, 4*bn)
	:~ ok <-> sk (es, 0.5*fs)
	:~ ck1 <-> sk (fs, es)
	CONSERVE ck4 + ck3 + ck2 + ck1 + ok = 1
	
	~ cna7 <-> cna6 (3*am, bm)
	~ cna6 <-> cna5 (2*am, 2*bm)
	~ cna5 <-> cna4 (am, 3*bm)
	~ cna3 <-> cna2 (3*am, bm)
	~ cna2 <-> cna1 (2*am, 2*bm)
	~ cna1 <-> ona (am, 3*bm)
	~ cna7 <-> cna3 (ah, bh)
	~ cna6 <-> cna2 (ah, bh)
	~ cna5 <-> cna1 (ah, bh)
	~ cna4 <-> ona (ah, bh)
	~ ona <-> s1 (as, 0)
	~ s1 <-> cna7 (bs, 0)
	~ s1 <-> s2 (bs, bs)
	~ s2 <-> s3 (bs, bs)
	~ s3 <-> s4 (bs, bs)
	~ s4 <-> s5 (bs, bs)
	~ s5 <-> s6 (bs, bs)
	~ s6 <-> s7 (bs, bs)
	~ s7 <-> s8 (bs, bs)
	~ s8 <-> s9 (bs, bs)
	~ s9 <-> s_10 (bs, bs)
	~ s_10 <-> s11 (bs, bs)
	~ s11 <-> s12 (bs, bs)
	~ s12 <-> s13 (bs, bs)
	~ s13 <-> s14 (bs, bs)
	~ s14 <-> s15 (bs, bs)
	~ s15 <-> s16 (bs, bs)
	~ s16 <-> s17 (bs, bs)
	~ s17 <-> s18 (bs, bs)
	~ s18 <-> s19 (bs, bs)
	~ s19 <-> s_20 (bs, bs)
	~ s_20 <-> s21 (bs, bs)
	~ s21 <-> s22 (bs, bs)
	~ s22 <-> s23 (bs, bs)
	~ s23 <-> s24 (bs, bs)
	~ s24 <-> s25 (bs, bs)
	~ s25 <-> s26 (bs, bs)
	~ s26 <-> s27 (bs, bs)
	~ s27 <-> s28 (bs, bs)
	~ s28 <-> s29 (bs, bs)
	~ s29 <-> s_30 (bs, bs)
	~ s_30 <-> s31 (bs, bs)
	~ s31 <-> s32 (bs, bs)
	~ s32 <-> s33 (bs, bs)
	~ s33 <-> s34 (bs, bs)
	~ s34 <-> s35 (bs, bs)
	~ s35 <-> s36 (bs, bs)
	~ s36 <-> s37 (bs, bs)
	~ s37 <-> s38 (bs, bs)
	~ s38 <-> s39 (bs, bs)
	~ s39 <-> s_40 (bs, bs)
	~ s_40 <-> s41 (bs, bs)
	~ s41 <-> s42 (bs, bs)
	~ s42 <-> s43 (bs, bs)
	~ s43 <-> s44 (bs, bs)
	~ s44 <-> s45 (bs, bs)
	~ s45 <-> s46 (bs, bs)
	~ s46 <-> s47 (bs, bs)
	~ s47 <-> s48 (bs, bs)
	~ s48 <-> s49 (bs, bs)
	~ s49 <-> s_50 (bs, bs)
	~ s_50 <-> s51 (bs, bs)
	~ s51 <-> s52 (bs, bs)
	~ s52 <-> s53 (bs, bs)
	~ s53 <-> s54 (bs, bs)
	~ s54 <-> s55 (bs, bs)
	~ s55 <-> s56 (bs, bs)
	~ s56 <-> s57 (bs, bs)
	~ s57 <-> s58 (bs, bs)
	~ s58 <-> s59 (bs, bs)
	~ s59 <-> s_60 (bs, bs)
	~ s_60 <-> s61 (bs, bs)
	~ s61 <-> s62 (bs, bs)
	~ s62 <-> s63 (bs, bs)
	~ s63 <-> s64 (bs, bs)
	~ s64 <-> s65 (bs, bs)
	~ s65 <-> s66 (bs, bs)
	~ s66 <-> s67 (bs, bs)
	~ s67 <-> s68 (bs, bs)
	~ s68 <-> s69 (bs, bs)
	~ s69 <-> s_70 (bs, bs)
	~ s_70 <-> s71 (bs, bs)
	~ s71 <-> s72 (bs, bs)
	~ s72 <-> s73 (bs, bs)
	~ s73 <-> s74 (bs, bs)
	~ s74 <-> s75 (bs, bs)
	~ s75 <-> s76 (bs, bs)
	~ s76 <-> s77 (bs, bs)
	~ s77 <-> s78 (bs, bs)
	~ s78 <-> s79 (bs, bs)
	~ s79 <-> s_80 (bs, bs)
	~ s_80 <-> s81 (bs, bs)
	~ s81 <-> s82 (bs, bs)
	~ s82 <-> s83 (bs, bs)
	~ s83 <-> s84 (bs, bs)
	~ s84 <-> s85 (bs, bs)
	~ s85 <-> s86 (bs, bs)
	~ s86 <-> s87 (bs, bs)
	~ s87 <-> s88 (bs, bs)
	~ s88 <-> s89 (bs, bs)
	~ s89 <-> s_90 (bs, bs)
	~ s_90 <-> s91 (bs, bs)
	~ s91 <-> s92 (bs, bs)
	~ s92 <-> s93 (bs, bs)
	~ s93 <-> s94 (bs, bs)
	~ s94 <-> s95 (bs, bs)
	~ s95 <-> s96 (bs, bs)
	~ s96 <-> s97 (bs, bs)
	~ s97 <-> s98 (bs, bs)
	~ s98 <-> s99 (bs, bs)
	:~ s99 <-> s_100 (bs, bs)
	CONSERVE cna7 + cna6 + cna5 + cna4 + cna3 + cna2 + cna1 + ona + s1 + s2 + 
	s3 + s4 + s5 + s6 + s7 + s8 + s9 + s_10 + s11 + s12 + s13 + s14 + s15 + 
	s16 + s17 + s18 + s19 + s_20 + s21 + s22 + s23 + s24 + s25 + s26 + s27 + 
	s28 + s29 + s_30 + s31 + s32 + s33 + s34 + s35 + s36 + s37 + s38 + s39 + 
	s_40 + s41 + s42 + s43 + s44 + s45 + s46 + s47 + s48 + s49 + s_50 + s51 + 
	s52 + s53 + s54 + s55 + s56 + s57 + s58 + s59 + s_60 + s61 + s62 + s63 + 
	s64 + s65 + s66 + s67 + s68 + s69 + s_70 + s71 + s72 + s73 + s74 + s75 + 
	s76 + s77 + s78 + s79 + s_80 + s81 + s82 + s83 + s84 + s85 + s86 + s87 + 
	s88 + s89 + s_90 + s91 + s92 + s93 + s94 + s95 + s96 + s97 + s98 + s99 = 1
}

 
INITIAL {
	rates(v)
    cna7 = 1
    cna6 = 0
    cna5 = 0
    cna4 = 0
    cna3 = 0
    cna2 = 0
    cna1 = 0
    ona = 0
    s1 =0
    s2 =0
    s3 =0
    s4 =0
    s5 =0
    s6 =0
    s7 =0
    s8 =0
    s9 =0
    s_10 =0
    s11 =0
    s12 =0
    s13 =0
    s14 =0
    s15 =0
    s16 =0
    s17 =0
    s18 =0
    s19 =0
    s_20 =0
    s21 =0
    s22 =0
    s23 =0
    s24 =0
    s25 =0
    s26 =0
    s27 =0
    s28 =0
    s29 =0
    s_30 =0
    s31 =0
    s32 =0
    s33 =0
    s34 =0
    s35 =0
    s36 =0
    s37 =0
    s38 =0
    s39 =0
    s_40 =0
    s41 =0
    s42 =0
    s43 =0
    s44 =0
    s45 =0
    s46 =0
    s47 =0
    s48 =0
    s49 =0
    s_50 =0
    s51 =0
    s52 =0
    s53 =0
    s54 =0 
    s55 =0
    s56 =0
    s57 =0
    s58 =0
    s59 =0
    s_60 =0
    s61 =0
    s62 =0
    s63 =0
    s64 =0
    s65 =0
    s66 =0
    s67 =0
    s68 =0
    s69 =0
    s_70 =0
    s71 =0
    s72 =0
    s73 =0
    s74 =0
    s75 =0
    s76 =0
    s77 =0
    s78 =0
    s79 =0
    s_80 =0   
    s81 =0
    s82 =0
    s83 =0
    s84 =0
    s85 =0
    s86 =0
    s87 =0
    s88 =0
    s89 =0
    s_90 =0
    s91 =0
    s92 =0
    s93 =0 
    s94 =0
    s95 =0
    s96 =0
    s97 =0
    s98 =0
    s99 =0
    :s_100 =0
    
    ck4 = 1
    ck3 = 0
    ck2 = 0
    ck1 = 0
    :sk = 0
    ok = 0
}


? rates
PROCEDURE rates(v(mV)) {  :Computes rate and other constants at current v.
	LOCAL q10
        TABLE am, bm, ah, bh, an, bn FROM -100 TO 100 WITH 200

UNITSOFF
	q10 = 3^(0.1*(celsius-T0))
        :"m" sodium activation system
    	am = 0.38*(v+29.7)/(1-exp(-0.1*(v+29.7)))
    	bm =  15.2*exp(-0.0556*(v+54.7))
	am = q10 * am
	bm = q10 * bm
    	:"h" sodium inactivation system
    	ah = 0.266*exp(-0.05*(v+48))
    	bh = 3.8/(1+exp(-0.1*(v+18)))
	ah = q10 * ah
	bh = q10 * bh
    	:"n" potassium activation system
    	an = 0.02*(v+45.7)/(1-exp(-0.1*(v+45.7)))
   	bn = 0.25*exp(-0.0125*(v+55.7))
	an = q10 * an
	bn = q10 * bn
        as = q10 * 0.1
        bs = q10 * 0.01
        :fs = 0.01
        :es = 0
UNITSON
}

UNITSOFF
FUNCTION vtrap(x,y) {  :Traps for 0 in denominator of rate eqns.
        if (fabs(x/y) < 1e-6) {
                vtrap = y*(1 - x/y/2)
        }else{
                vtrap = x/(exp(x/y) - 1)
        }
}
 
UNITSON
