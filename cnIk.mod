
TITLE Delayed rectifier potassium current with channel noise (Markov chain version)

COMMENT

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: March 2012

ENDCOMMENT

UNITS {
    (mA) = (milliamp)
    (mV) = (millivolt)
    (S)  = (siemens)
    (pS) = (picosiemens)
    (um) = (micron)
}

NEURON {
    SUFFIX cnIk
    USEION k READ ek WRITE ik
    RANGE gkbar, gk, gammak, Nk
    RANGE seed
    THREADSAFE
}

DEFINE CNK_NSTATES         5
DEFINE CNK_NSTATES_SQUARE 25
DEFINE CNK_OPEN            4

PARAMETER {
    gkbar  = 0.036    (S/cm2)    : maximum sodium conductance
    gammak = 10      (pS)	 : single channel sodium conductance
    seed    = 5061983 (1)        : always use the same seed
}

STATE {
    spam
    channels[CNK_NSTATES]        (1)
}


ASSIGNED {
    ik    (mA/cm2)
    gk    (S/cm2)
    ek    (mV)
    dt    (ms)
    area  (um2)
    v     (mV)
    Nk    (1)
    channels_now[CNK_NSTATES]     (1)
    A[CNK_NSTATES_SQUARE]         (1)
}

INITIAL {
    spam = 0
    Nk = ceil(((1e-8)*area)*(gkbar)/((1e-12)*gammak))
    printf("[Ik] number of channels = %.0f.\n", Nk)
    FROM i=0 TO CNK_NSTATES-1 {
	channels[i] = 0
    }
    channels[0] = Nk
    set_seed(seed)
}


BREAKPOINT {
    SOLVE states METHOD cnexp
    :gk = channels[CNK_OPEN]*((1e-12)*gammak)/((1e-8)*area)
    gk = gkbar * channels[CNK_OPEN] / Nk
    ik = gk * (v-ek)
}


DERIVATIVE states {
    spam' = spam
    fill_transition_matrix()
    evolve_channels()
}

PROCEDURE evolve_channels() {
    LOCAL escape_rate,transition_probability,fraction,nchannels,rnd
    FROM i=0 TO CNK_NSTATES-1 {
	channels_now[i] = channels[i]
    }
    
    FROM i=0 TO CNK_NSTATES-1 {
	escape_rate = 0
	FROM j=0 TO CNK_NSTATES-1 {
	    if (i != j) {
		escape_rate = escape_rate + A[i*CNK_NSTATES+j]
	    }
	}
	transition_probability = dt*escape_rate : probability of changing state
	nchannels = channels_now[i]
	FROM n=1 TO nchannels {
	    rnd = scop_random()	    
	    if (rnd <= transition_probability) { : the n-th channel changes state
		fraction = 0
		FROM j=0 TO CNK_NSTATES-1 {
		    if (i != j) {
			fraction = fraction + A[i*CNK_NSTATES+j] * dt
			if (rnd <= fraction) {
			    channels[i] = channels[i]-1
			    channels[j] = channels[j]+1
			    VERBATIM
			    break;
			    ENDVERBATIM
			}
		    }
		}
	    }
	}
    }
}

PROCEDURE fill_transition_matrix() {
    LOCAL an,bn
    an = alphan(v)
    bn = betan(v)
    
    A[0] = -4*an
    A[1] = 4*an
    A[2] = 0.0
    A[3] = 0.0
    A[4] = 0.0

    A[5] = bn
    A[6] = -3*an-bn
    A[7] = 3*an
    A[8] = 0.0
    A[9] = 0.0

    A[10] = 0.0
    A[11] = 2*bn
    A[12] = -2*an-2*bn
    A[13] = 2*an
    A[14] = 0.0

    A[15] = 0.0
    A[16] = 0.0
    A[17] = 3*bn
    A[18] = -an-3*bn
    A[19] = an

    A[20] = 0.0
    A[21] = 0.0
    A[22] = 0.0
    A[23] = 4*bn
    A[24] = -4*bn
}

FUNCTION vtrap(x,y) {  :Traps for 0 in denominator of rate eqns.
    if (fabs(x/y) < 1e-6) {
        vtrap = y*(1 - x/y/2)
    }else{
        vtrap = x/(exp(x/y) - 1)
    }
}
 
FUNCTION alphan(Vm (mV)) (/ms) {
    UNITSOFF
    alphan = .01*vtrap(-(Vm+55),10) 
    UNITSON
}

FUNCTION betan(Vm (mV)) (/ms) {
    UNITSOFF
    betan = .125*exp(-(Vm+65)/80)
    UNITSON
}

