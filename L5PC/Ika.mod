
COMMENT

A-type potassium current.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

NEURON {
    SUFFIX Ika
    USEION k READ ek WRITE ik
    RANGE gkbar, thetaa, sigmaa, thetab, sigmab, taub
    RANGE gk
    THREADSAFE
}

PARAMETER {
    gkbar  =   0.0014  (S/cm2)
    thetaa = -50       (mV)
    sigmaa =  20       (mV)
    thetab = -80       (mV)
    sigmab = - 6       (mV)
    taub   =  15       (ms) 
}

ASSIGNED {
    v      (mV)
    ek     (mV)
    ik     (mA/cm2)
    gk     (S/cm2)
    binf   (1)
    ainf   (1)
}

STATE {
    b (1)
}

INITIAL {
    rates()
    b = binf
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gk = gkbar * ainf^3 * b
    ik = gk * (v-ek)
}

DERIVATIVE states {
    rates()
    b' = (binf - b) / taub
}

PROCEDURE rates() {
    UNITSOFF
    ainf = 1. / (1. + exp(-(v-thetaa)/sigmaa))
    binf = 1. / (1. + exp(-(v-thetab)/sigmab))
    UNITSON
}


