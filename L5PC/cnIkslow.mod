
COMMENT

Slow potassium current with channel noise.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
    (pS) = (picosiemens)
    (um) = (micrometer)
}

NEURON {
    SUFFIX cnIkslow
    USEION k READ ek WRITE ik
    RANGE gkbar, thetaz, sigmaz, tauz
    RANGE gk
    RANGE gamma, nchan, seed
    THREADSAFE
}

PARAMETER {
    gkbar  =   0.001   (S/cm2)
    thetaz = -39       (mV)
    sigmaz =   5       (mV)
    tauz   =  75       (ms)
    gamma  =  20       (pS)
    seed   =  5061983  (1)
}

ASSIGNED {
    v         (mV)
    ek        (mV)
    ik        (mA/cm2)
    gk        (S/cm2)
    zinf      (1)
    dt        (ms)
    area      (um2)
    nchan     (1)
    cn_mu     (1)
    cn_noise  (1)
    coeff     (1)
}

STATE {
    z    (1)
    cn_z (1)
}

INITIAL {
    nchan = ceil(((1e-8)*area)*(gkbar)/((1e-12)*gamma))
    coeff = 1. / nchan
    printf("cnIkslow>> number of channels: %.0f.\n", nchan)
    rates()
    z = zinf
    cn_z = 0
    set_seed(seed)
}

BREAKPOINT {
    SOLVE states
    gk = gkbar * (z + cn_z)
    if (gk < 0) {
	gk = 0
    }
    else if (gk > gkbar) {
	gk = gkbar
    }
    ik = gk * (v-ek)
}

PROCEDURE states() {
    rates()
    z = z + dt * (zinf - z) / tauz
    cn_z = cn_z * cn_mu + cn_noise
}

PROCEDURE rates() {
    LOCAL ss
    UNITSOFF
    zinf = 1. / (1. + exp(-(v-thetaz)/sigmaz))
    ss = coeff * zinf * (1-zinf)
    cn_mu = exp(-dt/tauz)
    cn_noise = sqrt(ss * (1-cn_mu*cn_mu)) * normrand(0,1)
    UNITSON
}
