
COMMENT

Slow potassium current.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

NEURON {
    SUFFIX Ikslow
    USEION k READ ek WRITE ik
    RANGE gkbar, thetaz, sigmaz, tauz
    RANGE gk
    THREADSAFE
}

PARAMETER {
    gkbar  =   0.001   (S/cm2)
    thetaz = -39       (mV)
    sigmaz =   5       (mV)
    tauz   =  75       (ms) 
}

ASSIGNED {
    v      (mV)
    ek     (mV)
    ik     (mA/cm2)
    gk     (S/cm2)
    zinf   (1)
}

STATE {
    z (1)
}

INITIAL {
    rates()
    z = zinf
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gk = gkbar * z
    ik = gk * (v-ek)
}

DERIVATIVE states {
    rates()
    z' = (zinf - z) / tauz
}

PROCEDURE rates() {
    UNITSOFF
    zinf = 1. / (1. + exp(-(v-thetaz)/sigmaz))
    UNITSON
}
