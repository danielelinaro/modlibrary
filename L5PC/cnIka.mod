
COMMENT

A-type potassium current with channel noise.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
    (pS) = (picosiemens)
    (um) = (micrometer)
}

NEURON {
    SUFFIX cnIka
    USEION k READ ek WRITE ik
    RANGE gkbar, thetaa, sigmaa, thetab, sigmab, taub
    RANGE gk
    RANGE gamma, nchan, seed, Z
    THREADSAFE
}

DEFINE CNIKA_N 7

PARAMETER {
    gkbar  =   0.0014  (S/cm2)
    thetaa = -50       (mV)
    sigmaa =  20       (mV)
    thetab = -80       (mV)
    sigmab = - 6       (mV)
    taub   =  15       (ms)
    gamma  =  20       (pS)
    seed   =  5061983  (1)
}

ASSIGNED {
    v      (mV)
    ek     (mV)
    ik     (mA/cm2)
    gk     (S/cm2)
    binf   (1)
    ainf   (1)
    dt                 (ms)
    area               (um2)
    nchan              (1)
    Z                  (1)
    cn_tau[CNIKA_N]    (ms)
    cn_var[CNIKA_N]    (1)
    cn_noise[CNIKA_N]  (1)
    cn_mu[CNIKA_N]     (1)
}

STATE {
    b (1)
    cn_z[CNIKA_N] (1)
}

INITIAL {
    nchan = ceil(((1e-8)*area)*(gkbar)/((1e-12)*gamma))
    printf("cnIka>> number of channels: %.0f.\n", nchan)
    rates()
    b = binf
    FROM i=0 TO CNIKA_N-1 {
	cn_z[i] = 0.0
    }
    set_seed(seed)
}

BREAKPOINT {
    SOLVE states
    Z = 0
    FROM i=0 TO CNIKA_N-1 {
	Z = Z + cn_z[i]
    }
    gk = gkbar * (ainf^3 * b + Z)
    if (gk < 0) {
	gk = 0
    }
    else if (gk > gkbar) {
	gk = gkbar
    }
    ik = gk * (v-ek)
}

PROCEDURE states() {
    rates()
    b = b + dt * (binf - b) / taub
    FROM i=0 TO CNIKA_N-1 {
	cn_z[i] = cn_z[i] * cn_mu[i] + cn_noise[i]
    }
}

PROCEDURE rates() {
    LOCAL ainf3,one_minus_ainf,one_minus_binf
    UNITSOFF
    ainf = 1. / (1. + exp(-(v-thetaa)/sigmaa))
    binf = 1. / (1. + exp(-(v-thetab)/sigmab))

    ainf3 = ainf^3
    one_minus_ainf = 1.0 - ainf
    one_minus_binf = 1.0 - binf
    
    cn_var[0] = 1.0 / nchan * ainf3*ainf3*binf * one_minus_binf
    cn_var[1] = 3.0 / nchan * ainf3*ainf*ainf*binf*binf * one_minus_ainf
    cn_var[2] = 3.0 / nchan * ainf3*ainf*binf*binf * one_minus_ainf*one_minus_ainf
    cn_var[3] = 1.0 / nchan * ainf3*binf*binf * one_minus_ainf*one_minus_ainf*one_minus_ainf
    cn_var[4] = 3.0 / nchan * ainf3*ainf*ainf*binf * one_minus_ainf*one_minus_binf
    cn_var[5] = 3.0 / nchan * ainf3*ainf*binf * one_minus_ainf*one_minus_ainf*one_minus_binf
    cn_var[6] = 1.0 / nchan * ainf3*binf * one_minus_ainf*one_minus_ainf*one_minus_ainf*one_minus_binf

    cn_tau[0] = taub
    cn_mu[0] = exp(-dt/cn_tau[0])
    FROM i=1 TO CNIKA_N-1 {
	cn_tau[i] = 0.0
	cn_mu[i] = 0.0
    }
    FROM i=0 TO CNIKA_N-1 {
	cn_noise[i] = sqrt(cn_var[i] * (1-cn_mu[i]*cn_mu[i])) * normrand(0,1)
    }
    UNITSON
}


