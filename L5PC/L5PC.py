#!/usr/bin/env python

import numpy as np
import pylab as p
from neuron import h
import nrnutils as nrn

L = 120
diam = 120
duration = 1000
amplitude = 0.5
delay = 500

#type = 'deterministic'
type = 'stochastic'
soma = nrn.makeL5PC(L, diam, type)
stim = nrn.makeIclamp(soma(0.5), duration, amplitude, delay)
lbls = {'v': '_ref_v',
        'gna': '_ref_gna_Ina','gnap': '_ref_gna_Inap',
        'gkdr': '_ref_gk_Ikdr', 'gka': '_ref_gk_Ika', 'gkslow': '_ref_gk_Ikslow'}
if type == 'stochastic':
    for k,v in lbls.items():
        if v.count('I') == 1:
            s = v.split('I')
            lbls[k] = s[0] + 'cnI' + s[1]
rec = nrn.makeRecorders(soma(0.5), lbls)
nrn.run(duration + 2*delay, dt=0.003)

step = 50
t = np.array(rec['t'])[0::step]
v = np.array(rec['v'])[0::step]
gna = np.array(rec['gna'])[0::step]
gnap = np.array(rec['gnap'])[0::step]
gkdr = np.array(rec['gkdr'])[0::step]
gka = np.array(rec['gka'])[0::step]
gkslow = np.array(rec['gkslow'])[0::step]

p.figure()
p.plot(t,v,'k')
p.xlabel('t (ms)')
p.ylabel('V (mV)')
p.figure()
p.plot(t,gna,'k',label='g_na')
p.plot(t,gnap,'r',label='g_nap')
p.plot(t,gkdr,'g',label='g_kdr')
p.plot(t,gka,'b',label='g_ka')
p.plot(t,gkslow,'y',label='g_kslow')
p.xlabel('t (ms)')
p.ylabel('g (S/cm2)')
p.legend(loc='best')
p.show()

#del stim
#R = nrn.computeInputResistance(soma(0.5), [-0.3,0.06,0.05], 1000, 500, dt=0.01, plot=False)
#print('Input resistance: %g MOhm.' % (R*1e-6))
