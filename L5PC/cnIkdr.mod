
COMMENT

Delayed rectifier potassium current with channel noise.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
    (pS) = (picosiemens)
    (um) = (micrometer)
}

NEURON {
    SUFFIX cnIkdr
    USEION k READ ek WRITE ik
    RANGE gkbar, thetan, sigman, thetant, sigmant
    RANGE gk
    RANGE gamma, nchan, seed, Z
    THREADSAFE
}

DEFINE CNIKDR_N 4

PARAMETER {
    gkbar   =   0.003   (S/cm2)
    thetan  = -30       (mV)
    sigman  =  10       (mV)
    thetant = -27       (mV)
    sigmant = -15       (mV)
    gamma  =  20       (pS)
    seed   =  5061983  (1)
}

ASSIGNED {
    v      (mV)
    ek     (mV)
    ik     (mA/cm2)
    gk     (S/cm2)
    ninf   (1)
    taun   (ms)
    dt                  (ms)
    area                (um2)
    nchan               (1)
    Z                   (1)
    cn_tau[CNIKDR_N]    (ms)
    cn_var[CNIKDR_N]    (1)
    cn_noise[CNIKDR_N]  (1)
    cn_mu[CNIKDR_N]     (1)
}

STATE {
    n (1)
    cn_z[CNIKDR_N] (1)
}

INITIAL {
    nchan = ceil(((1e-8)*area)*(gkbar)/((1e-12)*gamma))
    printf("cnIkdr>> number of channels: %.0f.\n", nchan)
    rates()
    n = ninf
    FROM i=0 TO CNIKDR_N-1 {
	cn_z[i] = 0.0
    }
    set_seed(seed)
}

BREAKPOINT {
    SOLVE states
    Z = 0
    FROM i=0 TO CNIKDR_N-1 {
	Z = Z + cn_z[i]
    }
    gk = gkbar * (n^4 + Z)
    if (gk < 0) {
	gk = 0
    }
    else if (gk > gkbar) {
	gk = gkbar
    }
    ik = gk * (v-ek)
}

PROCEDURE states() {
    rates()
    n = n + dt * (ninf - n) / taun
    FROM i=0 TO CNIKDR_N-1 {
	cn_z[i] = cn_z[i]*cn_mu[i] + cn_noise[i]
    }
}

PROCEDURE rates() {
    LOCAL ninf4, one_minus_ninf
    UNITSOFF
    ninf = 1. / (1. + exp(-(v-thetan)/sigman))
    taun = 0.37 + 1.85 / (1+exp(-(v-thetant)/sigmant))
    ninf4 = ninf^4
    one_minus_ninf = 1.0 - ninf
    cn_var[0] = 4.0/nchan * ninf4*ninf*ninf*ninf * one_minus_ninf
    cn_var[1] = 6.0/nchan * ninf4*ninf*ninf * one_minus_ninf*one_minus_ninf
    cn_var[2] = 4.0/nchan * ninf4*ninf * one_minus_ninf*one_minus_ninf*one_minus_ninf
    cn_var[3] = 1.0/nchan * ninf4 * one_minus_ninf*one_minus_ninf*one_minus_ninf*one_minus_ninf
    FROM i=0 TO CNIKDR_N-1 {
	cn_tau[i] = taun / (i+1)
	cn_mu[i] = exp(-dt/cn_tau[i])
	cn_noise[i] = sqrt(cn_var[i] * (1-cn_mu[i]*cn_mu[i])) * normrand(0,1)
    }
    UNITSON
}


