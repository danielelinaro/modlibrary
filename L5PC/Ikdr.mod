
COMMENT

Delayed rectifier potassium current.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

NEURON {
    SUFFIX Ikdr
    USEION k READ ek WRITE ik
    RANGE gkbar, thetan, sigman, thetant, sigmant
    RANGE gk
    THREADSAFE
}

PARAMETER {
    gkbar   =   0.003   (S/cm2)
    thetan  = -30       (mV)
    sigman  =  10       (mV)
    thetant = -27       (mV)
    sigmant = -15       (mV)
}

ASSIGNED {
    v      (mV)
    ek     (mV)
    ik     (mA/cm2)
    gk     (S/cm2)
    ninf   (1)
    taun   (ms)
}

STATE {
    n (1)
}

INITIAL {
    rates()
    n = ninf
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gk = gkbar * n^4
    ik = gk * (v-ek)
}

DERIVATIVE states {
    rates()
    n' = (ninf - n) / taun
}

PROCEDURE rates() {
    UNITSOFF
    ninf = 1. / (1. + exp(-(v-thetan)/sigman))
    taun = 0.37 + 1.85 / (1+exp(-(v-thetant)/sigmant))
    UNITSON
}


