
COMMENT

Fast sodium current with channel noise.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
    (pS) = (picosiemens)
    (um) = (micrometer)
}

NEURON {
    SUFFIX cnIna
    USEION na READ ena WRITE ina
    RANGE gnabar, thetam, sigmam, thetah, sigmah, thetaht, sigmaht
    RANGE gna
    RANGE gamma, nchan, seed, Z
    THREADSAFE
}

DEFINE CNINA_N 7

PARAMETER {
    gnabar  =   0.024   (S/cm2)
    thetam  = -30       (mV)
    sigmam  =   9.5     (mV)
    thetah  = -53       (mV)
    sigmah  = - 7       (mV)
    thetaht = -40.5     (mV)
    sigmaht = - 6       (mV)
    gamma  =  20       (pS)
    seed   =  5061983  (1)
}

ASSIGNED {
    v      (mV)
    ena    (mV)
    ina    (mA/cm2)
    gna    (S/cm2)
    minf   (1)
    hinf   (1)
    tauh   (ms)
    dt                 (ms)
    area               (um2)
    nchan              (1)
    Z                  (1)
    cn_tau[CNINA_N]    (ms)
    cn_var[CNINA_N]    (1)
    cn_noise[CNINA_N]  (1)
    cn_mu[CNINA_N]     (1)
}

STATE {
    h (1)
    cn_z[CNINA_N] (1)
}

INITIAL {
    nchan = ceil(((1e-8)*area)*(gnabar)/((1e-12)*gamma))
    printf("cnIna>> number of channels: %.0f.\n", nchan)
    rates()
    h = hinf
    FROM i=0 TO CNINA_N-1 {
	cn_z[i] = 0.0
    }
    set_seed(seed)
}

BREAKPOINT {
    SOLVE states
    Z = 0
    FROM i=0 TO CNINA_N-1 {
	Z = Z + cn_z[i]
    }
    gna = gnabar * (minf^3 * h + Z)
    if (gna < 0) {
	gna = 0
    }
    else if (gna > gnabar) {
	gna = gnabar
    }
    ina = gna * (v-ena)
}

PROCEDURE states() {
    rates()
    h = h + dt * (hinf - h) / tauh
    FROM i=0 TO CNINA_N-1 {
	cn_z[i] = cn_z[i] * cn_mu[i] + cn_noise[i]
    }
}

PROCEDURE rates() {
    LOCAL minf3,one_minus_minf,one_minus_hinf
    UNITSOFF
    minf = 1. / (1. + exp(-(v-thetam)/sigmam))
    hinf = 1. / (1. + exp(-(v-thetah)/sigmah))
    tauh = 0.37 + 2.78 / (1+exp(-(v-thetaht)/sigmaht))

    minf3 = minf^3
    one_minus_minf = 1.0 - minf
    one_minus_hinf = 1.0 - hinf
    
    cn_var[0] = 1.0 / nchan * minf3*minf3*hinf * one_minus_hinf
    cn_var[1] = 3.0 / nchan * minf3*minf*minf*hinf*hinf * one_minus_minf
    cn_var[2] = 3.0 / nchan * minf3*minf*hinf*hinf * one_minus_minf*one_minus_minf
    cn_var[3] = 1.0 / nchan * minf3*hinf*hinf * one_minus_minf*one_minus_minf*one_minus_minf
    cn_var[4] = 3.0 / nchan * minf3*minf*minf*hinf * one_minus_minf*one_minus_hinf
    cn_var[5] = 3.0 / nchan * minf3*minf*hinf * one_minus_minf*one_minus_minf*one_minus_hinf
    cn_var[6] = 1.0 / nchan * minf3*hinf * one_minus_minf*one_minus_minf*one_minus_minf*one_minus_hinf

    cn_tau[0] = tauh
    cn_mu[0] = exp(-dt/cn_tau[0])
    FROM i=1 TO CNINA_N-1 {
	cn_tau[i] = 0.0
	cn_mu[i] = 0.0
    }
    FROM i=0 TO CNINA_N-1 {
	cn_noise[i] = sqrt(cn_var[i] * (1-cn_mu[i]*cn_mu[i])) * normrand(0,1)
    }
    UNITSON
}


