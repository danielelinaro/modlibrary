
COMMENT

Persistent sodium current.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

NEURON {
    SUFFIX Inap
    USEION na READ ena WRITE ina
    RANGE gnabar, thetap, sigmap
    RANGE gna
    THREADSAFE
}

PARAMETER {
    gnabar  =   0.00007 (S/cm2)
    thetap  = -40       (mV)
    sigmap  =   5       (mV)
}

ASSIGNED {
    v      (mV)
    ena    (mV)
    ina    (mA/cm2)
    gna    (S/cm2)
    pinf   (1)
}

STATE {
    foo
}

INITIAL {
    foo = 0
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = gnabar * pinf
    ina = gna * (v-ena)
}

DERIVATIVE states {
    rates()
    foo' = 0
}

PROCEDURE rates() {
    UNITSOFF
    pinf = 1. / (1. + exp(-(v-thetap)/sigmap))
    UNITSON
}


