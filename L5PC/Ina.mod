
COMMENT

Fast sodium current.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

NEURON {
    SUFFIX Ina
    USEION na READ ena WRITE ina
    RANGE gnabar, thetam, sigmam, thetah, sigmah, thetaht, sigmaht
    RANGE gna
    THREADSAFE
}

PARAMETER {
    gnabar  =   0.024   (S/cm2)
    thetam  = -30       (mV)
    sigmam  =   9.5     (mV)
    thetah  = -53       (mV)
    sigmah  = - 7       (mV)
    thetaht = -40.5     (mV)
    sigmaht = - 6       (mV)
}

ASSIGNED {
    v      (mV)
    ena    (mV)
    ina    (mA/cm2)
    gna    (S/cm2)
    minf   (1)
    hinf   (1)
    tauh   (ms)
}

STATE {
    h (1)
}

INITIAL {
    rates()
    h = hinf
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = gnabar * minf^3 * h
    ina = gna * (v-ena)
}

DERIVATIVE states {
    rates()
    h' = (hinf - h) / tauh
}

PROCEDURE rates() {
    UNITSOFF
    minf = 1. / (1. + exp(-(v-thetam)/sigmam))
    hinf = 1. / (1. + exp(-(v-thetah)/sigmah))
    tauh = 0.37 + 2.78 / (1+exp(-(v-thetaht)/sigmaht))
    UNITSON
}


