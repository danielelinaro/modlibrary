
COMMENT

Persistent sodium current with channel noise.

Dynamics taken from:
Propagating Neuronal Discharges in Neocortical Slices: Computational and Experimental Study
David Golomb and Yael Amitai
J Neurophysiol 78:1199-1211, 1997

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
    (pS) = (picosiemens)
    (um) = (micrometer)
}

NEURON {
    SUFFIX cnInap
    USEION na READ ena WRITE ina
    RANGE gnabar, thetap, sigmap
    RANGE gna
    RANGE gamma, nchan, seed
    THREADSAFE
}

PARAMETER {
    gnabar  =   0.00007 (S/cm2)
    thetap  = -40       (mV)
    sigmap  =   5       (mV)
    gamma  =  20       (pS)
    seed   =  5061983  (1)
}

ASSIGNED {
    v         (mV)
    ena       (mV)
    ina       (mA/cm2)
    gna       (S/cm2)
    pinf      (1)
    dt        (ms)
    area      (um2)
    nchan     (1)
    coeff     (1)
}

STATE {
    foo (1)
}

INITIAL {
    nchan = ceil(((1e-8)*area)*(gnabar)/((1e-12)*gamma))
    coeff = 1./nchan
    printf("cnInap>> number of channels: %.0f.\n", nchan)
    foo = 0
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = gnabar * (pinf + sqrt(coeff * pinf * (1-pinf)) * normrand(0,1))
    if (gna < 0) {
	gna = 0
    }
    else if (gna > gnabar) {
	gna = gnabar
    }
    ina = gna * (v-ena)
}

DERIVATIVE states {
    rates()
    foo' = 0
}
    
PROCEDURE rates() {
    UNITSOFF
    pinf = 1. / (1. + exp(-(v-thetap)/sigmap))
    UNITSON
}


