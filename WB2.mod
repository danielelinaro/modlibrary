TITLE WB2.mod
 
COMMENT
ENDCOMMENT
 
UNITS {
    (mA) = (milliamp)
    (mV) = (millivolt)
    (S) = (siemens)
}
 
? interface
NEURON {
    SUFFIX WB2
    USEION na READ ena WRITE ina
    USEION k READ ek WRITE ik
    RANGE gnabar, gkbar, gl, el, gna, gk
    RANGE phi
    GLOBAL minf, hinf, ninf, tauh, taun
    THREADSAFE : assigned GLOBALs will be per thread
}

PARAMETER {
    gnabar  = 0.035  (mho/cm2)
    gkbar   = 0.015  (mho/cm2)
    phi     = 5.0
}
 
STATE {
    h n
}
 
ASSIGNED {
    v (mV)
    celsius (degC)
    ena (mV)
    ek (mV)
    
    gna (S/cm2)
    gk (S/cm2)
    ina (mA/cm2)
    ik (mA/cm2)
    minf hinf ninf
    tauh (ms) taun (ms)
}
 
? currents
BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = gnabar*minf*minf*minf*h
    ina = gna*(v - ena)
    gk = gkbar*n*n*n*n
    ik = gk*(v - ek)
}
 
 
INITIAL {
    rates(v)
    h = hinf
    n = ninf
}

? states
DERIVATIVE states {  
    rates(v)
    h' = (hinf-h)/tauh
    n' = (ninf-n)/taun
}
 

? rates
PROCEDURE rates(v(mV)) {
    LOCAL a,b
    UNITSOFF
    if (v == -35.) { 
	a = 1 
    }
    else {
        a = 0.1 * (v+35.) / ( 1. - exp(-(v+35)/10.) ) 
    }
    b = 4. * exp(-(v+60.)/18.)    
    minf = a / (a + b)

    a = phi * 0.07 * exp(-0.05*(v+58.))
    b = phi * 1.0 / ( 1. + exp(-0.1*(v+28)) )
    tauh = (1. / (a + b))
    hinf = a / (a + b)
    
    if (v == -34.) {
	a = phi * 0.1 
    }
    else {
        a = phi * 0.01 * (v+34.) / ( 1. - exp(-0.1*(v+34)) )
    }
    b = phi * 0.125 * exp(-(v+44)/80.)
    taun = (1. / (a + b))
    ninf = a / (a + b)
    UNITSON
}
 
