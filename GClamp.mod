COMMENT
Conductance clamp.
Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: February 2013
ENDCOMMENT

UNITS {
    (mV) = (millivolt)
    (nA) = (nanoamp)
    (S)  = (siemens)
    (nS)  = (nanosiemens)
    (um) = (micron)
}

NEURON {
    POINT_PROCESS GClamp
    RANGE g
    RANGE e, delay, dur
    ELECTRODE_CURRENT i
    THREADSAFE
}

PARAMETER {
    e              (mV)
    delay          (ms)
    dur            (ms)
}

ASSIGNED {
    g      (nS)
    i      (nA)
    v      (mV)
}

INITIAL {
    i = 0
}

BREAKPOINT {
    at_time(delay)
    at_time(delay+dur)
    if (t>delay && t<delay+dur) {
	i = g*(e-v)*(1e-3)
    } else {
	i = 0
    }
}
