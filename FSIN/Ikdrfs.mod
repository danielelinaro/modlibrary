
COMMENT

Delayed rectifier potassium current of the Kv3.1-Kv3.2 type.

Dynamics taken from:
Mechanisms of firing patterns in fast-spiking cortical interneurons
David Golomb, Karnit Donner, Liron Shacham, Dan Shlosberg, Yael Amitai, David Hansel
PLoS Comput Biol 3(8): e156

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

NEURON {
    SUFFIX Ikdrfs
    USEION k READ ek WRITE ik
    RANGE gkbar, thetan, sigman
    RANGE gk
    THREADSAFE
}

PARAMETER {
    gkbar   =   0.225   (S/cm2)
    thetan  = -12.4     (mV)
    sigman  =   6.8     (mV)
}

ASSIGNED {
    v      (mV)
    ek     (mV)
    ik     (mA/cm2)
    gk     (S/cm2)
    ninf   (1)
    taun   (ms)
}

STATE {
    n (1)
}

INITIAL {
    rates()
    n = ninf
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gk = gkbar * n^2
    ik = gk * (v-ek)
}

DERIVATIVE states {
    rates()
    n' = (ninf - n) / taun
}

PROCEDURE rates() {
    UNITSOFF
    ninf = 1. / (1. + exp(-(v-thetan)/sigman))
    taun = (0.087 + 11.4 / (1+exp((v+14.6)/8.6))) * (0.087 + 11.4 / (1+exp(-(v-1.3)/18.7)))
    UNITSON
}


