
COMMENT

Fast sodium current for fast spiking cortical interneurons.

Dynamics taken from:
Mechanisms of firing patterns in fast-spiking cortical interneurons
David Golomb, Karnit Donner, Liron Shacham, Dan Shlosberg, Yael Amitai, David Hansel
PLoS Comput Biol 3(8): e156

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

NEURON {
    SUFFIX Inafs
    USEION na READ ena WRITE ina
    RANGE gnabar, thetam, sigmam, thetah, sigmah, thetaht, sigmaht
    RANGE gna
    THREADSAFE
}

PARAMETER {
    gnabar  =   0.1125  (S/cm2)
    thetam  = -24       (mV)
    sigmam  =  11.5     (mV)
    thetah  = -58.3     (mV)
    sigmah  = - 6.7     (mV)
    thetaht = -60       (mV)
    sigmaht = -12       (mV)
}

ASSIGNED {
    v      (mV)
    ena    (mV)
    ina    (mA/cm2)
    gna    (S/cm2)
    minf   (1)
    hinf   (1)
    tauh   (ms)
}

STATE {
    h (1)
}

INITIAL {
    rates()
    h = hinf
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = gnabar * minf^3 * h
    ina = gna * (v-ena)
}

DERIVATIVE states {
    rates()
    h' = (hinf - h) / tauh
}

PROCEDURE rates() {
    UNITSOFF
    minf = 1. / (1. + exp(-(v-thetam)/sigmam))
    hinf = 1. / (1. + exp(-(v-thetah)/sigmah))
    tauh = 0.5 + 14 / (1+exp(-(v-thetaht)/sigmaht))
    UNITSON
}


