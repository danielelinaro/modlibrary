
COMMENT

Potassium current with fast activation and slow inactivation.
The default value of the parameter gkbar is such that the neuron
produces tonic spiking with no delay.

Dynamics taken from:
Mechanisms of firing patterns in fast-spiking cortical interneurons
David Golomb, Karnit Donner, Liron Shacham, Dan Shlosberg, Yael Amitai, David Hansel
PLoS Comput Biol 3(8): e156

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: May 2012

ENDCOMMENT

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

NEURON {
    SUFFIX Id
    USEION k READ ek WRITE ik
    RANGE gdbar, thetaa, sigmaa, taua, thetab, sigmab, taub
    RANGE gk
    THREADSAFE
}

PARAMETER {
    gkbar  =   0.0001  (S/cm2)
    thetaa = -50       (mV)
    sigmaa =  20       (mV)
    taua   =   2       (ms)
    thetab = -70       (mV)
    sigmab = - 6       (mV)
    taub   = 150       (ms) 
}

ASSIGNED {
    v      (mV)
    ek     (mV)
    ik     (mA/cm2)
    gk     (S/cm2)
    binf   (1)
    ainf   (1)
}

STATE {
    a (1)
    b (1)
}

INITIAL {
    rates()
    a = ainf
    b = binf
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    gk = gkbar * a^3 * b
    ik = gk * (v-ek)
}

DERIVATIVE states {
    rates()
    a' = (ainf - a) / taua
    b' = (binf - b) / taub
}

PROCEDURE rates() {
    UNITSOFF
    ainf = 1. / (1. + exp(-(v-thetaa)/sigmaa))
    binf = 1. / (1. + exp(-(v-thetab)/sigmab))
    UNITSON
}


