#!/usr/bin/env python

import numpy as np
import pylab as p
from neuron import h
import nrnutils as nrn

L = 30
diam = 30
duration = 1000
amplitude = 0.1
delay = 500

soma = nrn.makeFSIN(L, diam)
stim = nrn.makeIclamp(soma(0.5), duration, amplitude, delay)
lbls = {'v': '_ref_v',
        'gnafs': '_ref_gna_Inafs',
        'gkdrfs': '_ref_gk_Ikdrfs', 'gd': '_ref_gk_Id'}
rec = nrn.makeRecorders(soma(0.5), lbls)
nrn.run(duration + 2*delay)

t = np.array(rec['t'])
v = np.array(rec['v'])
gnafs = np.array(rec['gnafs'])
gkdrfs = np.array(rec['gkdrfs'])
gd = np.array(rec['gd'])

p.figure()
p.plot(t,v,'k')
p.xlabel('t (ms)')
p.ylabel('V (mV)')
p.figure()
p.plot(t,gnafs/np.max(gnafs),'k',label='g_na')
p.plot(t,gkdrfs/np.max(gkdrfs),'g',label='g_kdr')
p.plot(t,gd/np.max(gd),'b',label='g_d')
p.xlabel('t (ms)')
p.ylabel('Normalized g')
p.legend(loc='best')
p.show()
        
del stim
R = nrn.computeInputResistance(soma(0.5), [-0.3,0.06,0.05], 1000, 500, dt=0.01, plot=True)
print('Input resistance: %g MOhm.' % (R*1e-6))
