COMMENT
 Authors: Vincent Delattre & Michele Giugliano
 Simple implementation of the conductance-based model introduced by Wang & Buzsaki (1996).
 Wang XJ, Buzsaki G (1996) Gamma oscillation by synaptic inhibition in a hippocampal interneuronal network model. J Neurosci 16:6402-13
ENDCOMMENT

COMMENT
Author: Daniele Linaro
I've made some modifications to the code:
1) I've added phi as a parameter (as it appears in the reference paper)
2) I've polished a bit the code: in particular, the exact solution now should look readable
3) I've replaced some divisions with multiplications
ENDCOMMENT

INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
    SUFFIX WB
    USEION na READ ena WRITE ina
    USEION k READ ek WRITE ik
    RANGE gnabar, gkbar
    RANGE gna, gk
    RANGE m_inf, h_inf, n_inf
    RANGE tau_h, tau_n, tau_m
    RANGE h_exp, n_exp, m_exp
    RANGE phi
} : end NEURON


UNITS {
    (S) = (mho)
    (mA) = (milliamp)
    (mV) = (millivolt)
} : end UNITS


PARAMETER {
    gnabar  = 0.035  (mho/cm2)
    gkbar   = 0.015  (mho/cm2)
    
    ena   (mV)
    ek    (mV)
    
    phi     = 5.0
    celsius        (degC)
    dt               (ms)
    v                (mV)
} : end PARAMETER


STATE {
    h n
} : end STATE


ASSIGNED {
    ina (mA/cm2)
    ik  (mA/cm2)
    gna (S/cm2)
    gk  (S/cm2)
    m_inf
    h_inf
    n_inf
    tau_h (ms)
    tau_n (ms)
    h_exp
    n_exp
} : end ASSIGNED


BREAKPOINT {
    SOLVE states
    gna = gnabar * m_inf * m_inf * m_inf * h
    gk = gkbar * n * n * n * n
    ina = gna * (v - ena)
    ik  = gk * (v - ek)
} : end BREAKPOINT


PROCEDURE states() {    : exact when v held constant
    evaluate_fct(v)
:    h = h + h_exp * (h_inf - h)
:    n = n + n_exp * (n_inf - n)
    h = h_inf + (h - h_inf) * h_exp
    n = n_inf + (n - n_inf) * n_exp
    VERBATIM
    return 0;
    ENDVERBATIM
} : end PROCEDURE states()

UNITSOFF

INITIAL {
    h = 0
    n = 0
} : end INITIAL


PROCEDURE evaluate_fct(v(mV)) { LOCAL a,b
    if (v == -35) { 
	a = 1 
    }
    else {
        a = 0.1 * (v+35.) / ( 1. - exp(-(v+35)/10.) ) 
    }
    b = 4. * exp(-(v+60.)/18.)    
    m_inf = a / (a + b)

    a = phi * 0.07 * exp(-0.05*(v+58.))
    b = phi * 1.0 / ( 1. + exp(-0.1*(v+28)) )
    tau_h = (1. / (a + b))
    h_inf = a / (a + b)
    
    if (v == -34) {
	a = phi * 0.1 
    }
    else {
        a = phi * 0.01 * (v+34.) / ( 1. - exp(-0.1*(v+34)) )
    }
    b = phi * 0.125 * exp(-(v+44)/80.)
    tau_n = (1. / (a + b))
    n_inf = a / (a + b)
    
:    h_exp = 1 - exp(-dt/tau_h)
:    n_exp = 1 - exp(-dt/tau_n)
    h_exp = exp(-dt/tau_h)
    n_exp = exp(-dt/tau_n)
    
} : end PROCEDURE evaluate_fct()

UNITSON
