COMMENT
Current clamp that generates a doublet of stimulations

Author: Daniele Linaro
Date: January 4, 2009
ENDCOMMENT

NEURON {
    POINT_PROCESS IClampDoublet
    RANGE delay1, dur1, amp1, delay2, dur2, amp2, i
    ELECTRODE_CURRENT i
}

UNITS {
    (nA) = (nanoamp)
}

PARAMETER {
    delay1  (ms)
    dur1  (ms)
    amp1  (nA)
    delay2  (ms)
    dur2  (ms)
    amp2  (nA)
}

ASSIGNED { i (nA) }

INITIAL { i = 0.0 }

BREAKPOINT {
    at_time(delay1)
    at_time(delay1+dur1)
    at_time(delay2)
    at_time(delay2+dur2)
    if (t>delay1 && t<delay1+dur1) {
	i = amp1
    } else if (t>delay2 && t<delay2+dur2) {
	i = amp2
    } else {
	i = 0
    }
}

