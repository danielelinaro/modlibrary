
TITLE Sodium current with a chain of inactivated states plus channel noise (Markov chain version)

COMMENT

Author: Daniele Linaro - daniele@tnb.ua.ac.be
Date: March 2012

ENDCOMMENT

VERBATIM
#include <stdlib.h>
ENDVERBATIM

UNITS {
    (mA) = (milliamp)
    (mV) = (millivolt)
    (S)  = (siemens)
    (pS) = (picosiemens)
    (um) = (micron)
}

NEURON {
    SUFFIX cnInaic
    USEION na READ ena WRITE ina
    RANGE gnabar, gna, gammana, Nna
    RANGE seed
    THREADSAFE
}

DEFINE CNNAIC_NSTATES           108
DEFINE CNNAIC_NSTATES_SQUARE  11664
DEFINE CNNAIC_OPEN                7

PARAMETER {
    gnabar  = 0.12    (S/cm2)    : maximum sodium conductance
    gammana = 10      (pS)	 : single channel sodium conductance
    seed    = 5061983 (1)        : always use the same seed
    alpha   = 0.1     (/ms)
    beta    = 0.01    (/ms)
}

STATE {
    spam
    channels[CNNAIC_NSTATES]        (1)
}


ASSIGNED {
    ina    (mA/cm2)
    gna    (S/cm2)
    ena    (mV)
    dt     (ms)
    area   (um2)
    v      (mV)
    Nna    (1)
    channels_now[CNNAIC_NSTATES]     (1)
    A[CNNAIC_NSTATES_SQUARE]         (1)
    B[64]                            (1)
}

INITIAL {
    spam = 0
    Nna = ceil(((1e-8)*area)*(gnabar)/((1e-12)*gammana))
    printf("[Inaic] number of channels = %.0f.\n", Nna)
    FROM i=0 TO CNNAIC_NSTATES-1 {
	channels[i] = 0
    }
    channels[4] = Nna
    FROM i=0 TO CNNAIC_NSTATES_SQUARE {
	A[i] = 0.0
    }
    FROM i=0 TO 63 {
        B[i] = 0.0
    }
    set_seed(seed)
    fill_transition_matrix()
}


BREAKPOINT {
    SOLVE states METHOD cnexp
    gna = channels[CNNAIC_OPEN]*((1e-12)*gammana)/((1e-8)*area)
    ina = gna * (v-ena)
}


DERIVATIVE states {
    spam' = spam
    fill_transition_matrix()
    evolve_channels()
}


PROCEDURE evolve_channels() {
    LOCAL escape_rate,transition_probability,fraction,nchannels,one_over_escape_rate,rnd
    FROM i=0 TO CNNAIC_NSTATES-1 {
	channels_now[i] = channels[i]
    }
    
    FROM i=0 TO CNNAIC_NSTATES-1 {
	escape_rate = 0
	FROM j=0 TO CNNAIC_NSTATES-1 {
	    if (i != j) {
		escape_rate = escape_rate + A[i*CNNAIC_NSTATES+j]
	    }
	}
	transition_probability = dt*escape_rate : probability of changing state
	nchannels = channels_now[i]
	FROM n=1 TO nchannels {
	    rnd = scop_random()	    
	    if (rnd <= transition_probability) { : the n-th channel changes state
		fraction = 0
		FROM j=0 TO CNNAIC_NSTATES-1 {
		    if (i != j) {
			fraction = fraction + A[i*CNNAIC_NSTATES+j] * dt
			if (rnd <= fraction) {
			    channels[i] = channels[i]-1
			    channels[j] = channels[j]+1
			    VERBATIM
			    break;
			    ENDVERBATIM
			}
		    }
		}
	    }
	}
    }
}

PROCEDURE fill_transition_matrix() {
    LOCAL am,bm,ah,bh
    am = alpham(v)
    bm = betam(v)
    ah = alphah(v)
    bh = betah(v)
    
    : first block (transition rates matrix of standard inactivating sodium channels)
    B[0] = -ah-3.*am
    B[1] = 3.*am
    B[4] = ah
    B[8] = bm
    B[9] = -ah-2.*am-bm
    B[10] = 2.*am
    B[13] = ah
    B[17] = 2.*bm
    B[18] = -ah-am-2.*bm
    B[19] = am
    B[22] = ah
    B[26] = 3.*bm
    B[27] = -ah-3.*bm
    B[31] = ah
    B[32] = bh
    B[36] = -3.*am-bh
    B[37] = 3.*am
    B[41] = bh
    B[44] = bm
    B[45] = -2.*am-bh-bm
    B[46] = 2.*am
    B[50] = bh
    B[53] = 2.*bm
    B[54] = -am-bh-2.*bm
    B[55] = am
    B[59] = bh
    B[62] = 3.*bm
    B[63] = -bh-3.*bm
    
    FROM i=0 TO 7 {
	FROM j=0 TO 7 {
	    A[i*CNNAIC_NSTATES+j] = B[i*8+j]
	}
    }
    
    A[7*CNNAIC_NSTATES+7]   = A[7*CNNAIC_NSTATES+7] - alpha
    A[7*CNNAIC_NSTATES+7+1] =    alpha
    A[8*CNNAIC_NSTATES]     =    beta
    A[8*CNNAIC_NSTATES+8]   = -2*beta
    A[8*CNNAIC_NSTATES+8+1] =    beta
    
    FROM i=9 TO CNNAIC_NSTATES-2 {
	A[i*CNNAIC_NSTATES+i-1] =    beta
	A[i*CNNAIC_NSTATES+i]   = -2*beta
	A[i*CNNAIC_NSTATES+i+1] =    beta
    }
    
    A[CNNAIC_NSTATES_SQUARE-2] = beta
    A[CNNAIC_NSTATES_SQUARE-1] = -beta
    
    :printf("V = %g\n", v)
    :FROM i=0 TO CNNAIC_NSTATES-1 {
    :	FROM j=0 TO CNNAIC_NSTATES-1 {
    :	    printf("%12.6g ", A[j*CNNAIC_NSTATES+i])
    :	}
    :	printf("\n")
    :}
    
}

FUNCTION vtrap(x,y) {  :Traps for 0 in denominator of rate eqns.
    if (fabs(x/y) < 1e-6) {
        vtrap = y*(1 - x/y/2)
    }else{
        vtrap = x/(exp(x/y) - 1)
    }
}
 

FUNCTION alpham(Vm (mV)) (/ms) {
    UNITSOFF
    alpham = .1 * vtrap(-(Vm+40),10)
    UNITSON
}


FUNCTION betam(Vm (mV)) (/ms) {
    UNITSOFF
    betam =  4 * exp(-(Vm+65)/18)
    UNITSON
}


FUNCTION alphah(Vm (mV)) (/ms) {
    UNITSOFF
    alphah = .07 * exp(-(Vm+65)/20)
    UNITSON
}


FUNCTION betah(Vm (mV)) (/ms) {
    UNITSOFF
    betah = 1 / (exp(-(Vm+35)/10) + 1)
    UNITSON
}

