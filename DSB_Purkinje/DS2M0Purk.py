#!/usr/bin/env python

# De Schutter model conductances in De Schutter-Rapp cell
# Conversion from hoc by Daniele Linaro
# October 2014

from neuron import h
import numpy as np
import pylab as p
import time
import sys
import os
import h5py as h5
import itertools as it
import pdb

def save_dict(fid, group, data):
    for key,value in data.iteritems():
        if isinstance(value, dict):
            new_group = fid.create_group(group.name + '/' + key)
            save_dict(fid, new_group, value)
        elif type(value) in (int,float,tuple,str):
            group.attrs.create(key,value)
        else:
            group.create_dataset(key, data=np.array(value), compression='gzip', compression_opts=9)

def save_h5_file(filename, **kwargs):
    with h5.File(filename, 'w') as fid:
        save_dict(fid, fid, kwargs)

def make_output_filename(prefix='', extension='.out'):
    filename = prefix
    if prefix != '' and prefix[-1] != '_':
        filename = filename + '_'
    now = time.localtime(time.time())
    filename = filename + '%d%02d%02d-%02d%02d%02d' % \
        (now.tm_year, now.tm_mon, now.tm_mday, now.tm_hour, now.tm_min, now.tm_sec)
    if extension[0] != '.':
        extension = '.' + extension
    suffix = ''
    k = 0
    while os.path.exists(filename + suffix + extension):
        k = k+1
        suffix = '_%d' % k
    return filename + suffix + extension

class DS2M0Purk:
    def __init__(self, g_granule=0.7e-3, g_stellate=[7000,1400]):
        self._init_morphology()
        self._insert_passive_mechanisms()
        self._insert_active_mechanisms()
        self._soma.push() # make the soma the currently accessed section
        if not g_granule is None and not g_stellate is None:
            self._add_synapses(g_granule, g_stellate)

    def add_offset_current(self, amp=-0.01):
        # an offset current to prevent the cell from spiking
        self._offset = h.IClamp(self._soma(0.5))
        self._offset.delay = 0
        self._offset.amp = amp
        self._offset.dur = 1e8

    def _init_morphology(self):
        h('xopen("Purk2M0.nrn")')
        self._soma = h.soma
        self._dendrites = h.dend
        self._nsections = len(self._dendrites) + 1 # the number of sections in the dendrites, plus the soma
        self._main_dendrites = []
        for sec in h.md:
            self._main_dendrites.append(sec)
        self._thick_dendrites = []
        for sec in h.td:
            self._thick_dendrites.append(sec)
        self._thin_dendrites = []
        self._shell_depth = 0.2 # um
        self._main_dendrites_areas = []
        self._thick_dendrites_areas = []
        self._thin_dendrites_areas = []
        for i,sec in enumerate(self._dendrites):
            area = h.area(0.5,sec)
            if sec in self._main_dendrites:
                self._main_dendrites_areas.append(area)
            elif sec in self._thick_dendrites:
                self._thick_dendrites_areas.append(area)
            else:
                self._thin_dendrites.append(sec)
                self._thin_dendrites_areas.append(area)

    def _insert_passive_mechanisms(self):
        self._membrane_properties = {
            'capacitance': 1.64, # uF cm^-2
            'dendritic_resistance': 30000.,  # ohm cm^2
            'somatic_resistance': 10000., # ohm cm^2
            'axial_resistance': 250. # ohm cm
            }
        self._spine_density = 13
        self._spine_area = 1.33
        for sec in h.allsec():
            sec.insert('Leak')
            sec(0.5).Leak.el = -80.
            sec.Ra = self._membrane_properties['axial_resistance']
            sec.cm = 1.0 * self._membrane_properties['capacitance']
        for sec in self._dendrites:
            sec(0.5).Leak.gl = 1./self._membrane_properties['dendritic_resistance']
        for sec in self._main_dendrites:
            sec(0.5).Leak.gl = 1./self._membrane_properties['dendritic_resistance']
        self._soma(0.5).Leak.gl = 1./self._membrane_properties['somatic_resistance']
        for sec in self._dendrites:
            self._add_spines(sec)

    def _insert_active_mechanisms(self):
        ##### somatic ion channels
        soma = self._soma
        soma.insert('NaF')
        soma(0.5).NaF.gnabar = 7.5
        soma.insert('NaP')
        soma(0.5).NaP.gnabar = 0.001 # default is correct value for soma
        #soma.insert('CaP')
        #soma(0.5).CaP.gcabar = 0.0 # no P type Ca Ch in soma
        soma.insert('CaT')
        soma(0.5).CaT.gcabar = 0.0005 # default is correct value for soma and dend
        soma.insert('Kh1')
        soma(0.5).Kh1.gkbar = 0.0003 # default is correct value for soma, Ih uses only K ions here
        soma.insert('Kh2')
        soma(0.5).Kh2.gkbar = 0.0003 # default is correct value for soma, Ih uses only K ions here
        soma.insert('Kdr')
        soma(0.5).Kdr.gkbar = 0.6 # default is correct value for soma
        soma.insert('KMnew2')
        soma(0.5).KMnew2.gkbar = 0.00004 # default is correct value for soma
        soma.insert('KA')
        soma(0.5).KA.gkbar = 0.015 # default is correct value for soma
        #soma.insert('KC')
        #soma(0.5).KC.gkbar = 0.0 # no BK Ch in soma
        #soma.insert('K2')
        #soma(0.5).K2.gkbar = 0.0 # no KCa Ch in soma
        soma.insert('cad')
        # GENESIS calculates shell area as (outer circle area - inner circle area)
        # our calcium diffusion had used depth * outer circumference
        soma(0.5).cad.depth = self._shell_depth - self._shell_depth**2/soma.diam
        soma.cai = 4e-5
        soma.cao = 2.4
        soma.eca = 12.5 * np.log(self._soma.cao/self._soma.cai)
        h.ion_style('ca_ion', 1, 1, 0, 0, 0, sec=soma) # was ('ca_ion', 1, 1, 0, 1, 0)
        soma.ena = 45 # mV Na+ reversal potential
        soma.ek = -85 # mV K+ reversal potential
        
        ##### dendritic ion channels
        # gmax as in DeSchutter 1994 PM9 'rest of dendrite'
        for sec in self._dendrites:
            #sec.insert('NaF')
            #sec(0.5).NaF.gnabar = 0.
            #sec.insert('NaP')
            #sec(0.5).NaP.gnabar = 0.
            sec.insert('CaP')
            sec(0.5).CaP.gcabar = 0.0045
            sec.insert('CaT')
            sec(0.5).CaT.gcabar = 0.0005
            #sec.insert('Kh1')
            #sec(0.5).Kh1.gkbar = 0.
            #sec.insert('Kh2')
            #sec(0.5).Kh2.gkbar = 0.
            #sec.insert('Kdr')
            #sec(0.5).Kdr.gkbar = 0.
            sec.insert('KMnew2')
            sec(0.5).KMnew2.gkbar = 0.000013
            #sec.insert('KA')
            #sec(0.5).KA.gkbar = 0.
            sec.insert('KC')
            sec(0.5).KC.gkbar = 0.08
            sec.insert('K2')
            sec(0.5).K2.gkbar = 0.00039
            sec.insert('cad')
            # GENESIS calculates shell area as (outer circle area - inner circle area)
            # our calcium diffusion had used depth * outer circumference
            sec(0.5).cad.depth = self._shell_depth - self._shell_depth**2/sec.diam
            sec.cai = 4e-5
            sec.cao = 2.4
            h.ion_style('ca_ion', 1, 1, 1, 1, 0, sec=sec)
            sec.ek = -85 # mV K+ reversal potential

        # add (the same) conductances to main dend, the spine necks and spine heads
        # gmax as in DeSchutter 1994 PM9 'Main Dendrite'
        for sec in self._main_dendrites:
            sec(0.5).CaP.gcabar = 0.0045
            sec(0.5).CaT.gcabar = 0.0005
            sec.insert('Kdr')
            sec(0.5).Kdr.gkbar = 0.06
            sec(0.5).KMnew2.gkbar = 0.00001
            sec.insert('KA')
            sec(0.5).KA.gkbar = 0.002
            sec(0.5).KC.gkbar = 0.08
            sec(0.5).K2.gkbar = 0.00039
            sec(0.5).cad.depth = self._shell_depth - self._shell_depth**2/sec.diam
            sec.cai = 4e-5
            sec.cao = 2.4
            h.ion_style('ca_ion', 1, 1, 1, 1, 0, sec=sec) # was ('ca_ion', 1, 1, 0, 1, 0)
            sec.ek = -85 # mV K+ reversal potential
            
    def _spine_correction(self, sec):
        max_diam = 0
        for seg in sec:
            if seg.diam > max_diam:
                max_diam = seg.diam
        if max_diam <= 3.17: # spine correction only for thin dendrites
            area = 0
            for seg in sec:
                area = area + seg.area()
            corr = (sec.L * self._spine_area * self._spine_density + area) / area
            if not sec in self._thin_dendrites:
                print('Adding %s to the thin dendrites.' % h.secname(sec=sec))
                self._thin_dendrites.append(sec)
                self._thin_dendrites_areas.append(area)
        else:
            corr = 1.
        return corr
    
    def _add_spines(self, sec):
        corr = self._spine_correction(sec)
        sec(0.5).gl_Leak = corr / self._membrane_properties['dendritic_resistance']
        sec.cm = corr * self._membrane_properties['capacitance']

    def _distance_from_soma(self, sec):
        return self._distance(self._soma, sec)

    def _distance(self, origin, end, x=0.5):
        h.distance(sec=origin)
        return h.distance(x, sec=end)

    def _add_synapses(self,
                      g_granule = 0.7e-3, # [uS/cm2]
                      g_stellate = [7000.,1400.] # [us/cm2]
                      ):
        self.with_synapses = True
        print('Adding synapses:')
        # we model only excitatory synapses coming from granule cells and
        # inhibitory synapses coming from stellate and basket cells.
        self._synapses = {'granule_cells': [], 'stellate_cells': [], 'basket_cells': []}
        # one granule cell synapse per spiny dendritic compartment
        for sec in self.spiny_dendrites:
            s,st,nc = self.insert_synapse(sec, [0.5,1.2], 0., g_granule)
            self._synapses['granule_cells'].append({'syn': [s], 'stim': [st], 'conn': [nc], 'spike_times': [],
                                                    'w': g_granule, 'sec': sec, 'E': 0.,  'tau': [0.5,1.2]})
        print('   total conductance of granule cell synapses on the spiny dendrites: %g uS.' % \
                  (g_granule*len(self.spiny_dendrites)))
        # one stellate cell synapse per spiny dendritic compartment
        g_tot = 0
        for sec in self.spiny_dendrites:
            g = h.area(0.5,sec=sec) * 1e-8 * g_stellate[0]
            g_tot += g
            s,st,nc = self.insert_synapse(sec, [0.9,26.5], -80., g)
            self._synapses['stellate_cells'].append({'syn': [s], 'stim': [st], 'conn': [nc], 'spike_times': [],
                                                     'w': g, 'sec': sec, 'E': -80., 'tau': [0.9,26.5]})
        print('   total conductance of stellate cell synapses on the spiny dendrites: %g uS.' % g_tot)
        # two stellate cell synapses per smooth dendritic compartment
        g_tot = 0
        for sec in self.smooth_dendrites:
            for i in range(2):
                g = h.area(0.5,sec=sec) * 1e-8 * g_stellate[1]
                g_tot += g
                s,st,nc = self.insert_synapse(sec, [0.9,26.5], -80., g)
                self._synapses['stellate_cells'].append({'syn': [s], 'stim': [st], 'conn': [nc], 'spike_times': [],
                                                         'w': g, 'sec': sec, 'E': -80., 'tau': [0.9,26.5]})
        print('   total conductance of stellate cell synapses on the smooth dendrites: %g uS.' % g_tot)
        g_basket = 100 # [uS/cm2]
        g_tot = 0
        # some on the soma
        while g_tot < 0.139:
            g = h.area(0.5,sec=self.soma) * 1e-8 * g_basket
            g_tot += g
            s,st,nc = self.insert_synapse(self.soma, [0.9,26.5], -80., g)
            self._synapses['basket_cells'].append({'syn': [s], 'stim': [st], 'conn': [nc], 'spike_times': [],
                                                   'w': g, 'sec': sec, 'E': -80., 'tau': [0.9,26.5]})
        somatic_contacts = len(self._synapses['basket_cells'])
        print('   total conductance of basket cell synapses on the soma: %g uS (%d contacts).' % \
                  (g_tot,somatic_contacts))
        # the remaining on the main dendrite
        g_basket = 50 # [uS/cm2]
        g_tot = 0
        while g_tot < 0.047:
            for sec in self.main_dendrites:
                g = h.area(0.5,sec=self.soma) * 1e-8 * g_basket
                g_tot += g
                s,st,nc = self.insert_synapse(sec, [0.9,26.5], -80., g)
                self._synapses['basket_cells'].append({'syn': [s], 'stim': [st], 'conn': [nc], 'spike_times': [],
                                                       'w': g, 'sec': sec, 'E': -80., 'tau': [0.9,26.5]})
                if g_tot >= 0.047:
                    break
        print('   total conductance of basket cell synapses on the main dendrite: %g uS (%d contacts).' % \
                  (g_tot,len(self._synapses['basket_cells'])-somatic_contacts))
        # print the total numbers
        print('   number of granule cells synapses: %d.' % len(self._synapses['granule_cells']))
        print('   number of stellate cells synapses: %d.' % len(self._synapses['stellate_cells']))
        print('   number of basket cells synapses: %d.' % len(self._synapses['basket_cells']))

    def insert_synapse(self, sec, tau, E, w=0.0001, delay=0.):
        # the synapse
        s = h.Exp2Syn(sec(0.5))
        s.tau1 = tau[0]
        s.tau2 = tau[1]
        s.e = E
        st,nc = self.make_stim_and_conn(s, w, delay)
        return s,st,nc        

    def make_stim_and_conn(self, syn, w, delay=0.):
        # the vecstim
        st = h.VecStim()
        # the netcon
        nc = h.NetCon(st, syn)
        nc.weight[0] = w
        nc.delay = delay
        return st,nc

    def set_presynaptic_spike_times(self, synapse, spike_times):
        synapse['spike_times'].append(h.Vector(spike_times))
        synapse['stim'][0].play(synapse['spike_times'][-1])

    def pick_section(self, group, areas):
        tmp = np.cumsum(areas)
        return group[np.where(tmp > np.random.uniform(0,tmp[-1]))[0][0]]

    @property
    def soma(self):
        return self._soma
    
    @property
    def dendrites(self):
        return self._dendrites

    @property
    def main_dendrites(self):
        return self._main_dendrites

    @property
    def thick_dendrites(self):
        return self._thick_dendrites

    @property
    def smooth_dendrites(self):
        return self._thick_dendrites

    @property
    def thin_dendrites(self):
        return self._thin_dendrites
    
    @property
    def spiny_dendrites(self):
        return self._thin_dendrites

    @property
    def synapses(self):
        return self._synapses

def somatic_current_injection(cell, amplitude=3., tbefore=100., tstim=500., tafter=100., celsius=37.):
    Vrest = -68.
    print('Inserting stimulus...')
    stim = h.IClamp(cell.soma(0.5))
    stim.delay = tbefore
    stim.dur = tstim
    stim.amp = float(amplitude)
    print('Setting up recorders...')
    rec = {}
    for lbl in 't','vsoma','vdend','spikes':
        rec[lbl] = h.Vector()
    rec['t'].record(h._ref_t)
    rec['vsoma'].record(cell.soma(0.5)._ref_v)
    rec['vdend'].record(cell.dendrites[1511](0.5)._ref_v)
    apc = h.APCount(cell.soma(0.5))
    apc.record(rec['spikes'])
    print('Setting up the simulation...')
    h.load_file('stdrun.hoc')
    h.t = 0.
    h.dt = 0.02
    h.v_init = Vrest
    h.celsius = celsius
    h.tstop = tbefore + tstim + tafter
    print('Running model...')
    h.finitialize(Vrest)
    h.fcurrent()
    count = 0
    while h.t < h.tstop:
        h.fadvance()
        if count%10000 == 0:
            print h.t
        count += 1
    return np.array(rec['t'])*1e-3,np.array(rec['vsoma']),np.array(rec['vdend']),np.array(rec['spikes'])*1e-3

def synaptic_activation(cell, exc_rate, inh_rate, tstop=5000., seed=None):
    if not cell.with_synapses:
        raise Exception('No synapses present')

    cell.add_offset_current()
    
    if seed is None:
        np.random.seed(int(time.time()))
    else:
        np.random.seed(seed)

    print('Computing the activation times of the synapses...')
    if exc_rate > 0:
        nspikes = (tstop/1e3)*exc_rate
        for syn in cell.synapses['granule_cells']:
            isi = -np.log(np.random.uniform(size=nspikes)) / exc_rate * 1e3
            cell.set_presynaptic_spike_times(syn, np.cumsum(isi))
    if inh_rate > 0:
        nspikes = (tstop/1e3)*inh_rate
        for syn in cell.synapses['stellate_cells']:
            isi = -np.log(np.random.uniform(size=nspikes)) / inh_rate * 1e3
            cell.set_presynaptic_spike_times(syn, np.cumsum(isi))

    print('Setting up recorders...')
    rec = {}
    for lbl in 't','vsoma','vdend','spikes':
        rec[lbl] = h.Vector()
    rec['t'].record(h._ref_t)
    rec['vsoma'].record(cell.soma(0.5)._ref_v)
    rec['vdend'].record(cell.dendrites[1511](0.5)._ref_v)
    apc = h.APCount(cell.soma(0.5))
    apc.record(rec['spikes'])
    print('Setting up the simulation...')
    h.load_file('stdrun.hoc')
    h.t = 0.
    h.dt = 0.02
    h.v_init = -69.
    h.celsius = 37.
    h.tstop = tstop

    print('Running model...')
    h.finitialize(h.v_init)
    h.run()

    return np.array(rec['t'])*1e-3,np.array(rec['vsoma']),np.array(rec['vdend']),np.array(rec['spikes'])*1e-3

def compute_PRC(cell, exc_rate, inh_rate, firing_rate, seed, which_isi=0, ttran=3000., tstop=5000., ntrials=100, pulse_amp=0.5, pulse_dur=0.5):
    if not cell.with_synapses:
        raise Exception('No synapses present')

    cell.add_offset_current()
    
    np.random.seed(seed)

    print('Computing the activation times of the synapses...')
    presyn_spike_times = {}
    if exc_rate > 0:
        nspikes = (tstop/1e3)*exc_rate
        presyn_spike_times['exc'] = np.zeros((len(cell.synapses['granule_cells']),nspikes))
        for i,syn in enumerate(cell.synapses['granule_cells']):
            isi = -np.log(np.random.uniform(size=nspikes)) / exc_rate * 1e3
            cell.set_presynaptic_spike_times(syn, np.cumsum(isi))
            presyn_spike_times['exc'][i,:] = np.cumsum(isi)
    if inh_rate > 0:
        nspikes = (tstop/1e3)*inh_rate
        presyn_spike_times['inh'] = np.zeros((len(cell.synapses['stellate_cells']),nspikes))
        for i,syn in enumerate(cell.synapses['stellate_cells']):
            isi = -np.log(np.random.uniform(size=nspikes)) / inh_rate * 1e3
            cell.set_presynaptic_spike_times(syn, np.cumsum(isi))
            presyn_spike_times['inh'][i,:] = np.cumsum(isi)

    pulse = h.IClamp(cell.soma(0.5))
    pulse.delay = 1e8
    pulse.dur = pulse_dur
    pulse.amp = pulse_amp

    print('Setting up recorders...')
    rec = {}
    for lbl in 't','vsoma','spikes':
        rec[lbl] = h.Vector()
    rec['t'].record(h._ref_t)
    rec['vsoma'].record(cell.soma(0.5)._ref_v)
    apc = h.APCount(cell.soma(0.5))
    apc.record(rec['spikes'])

    print('Setting up the simulation...')
    h.load_file('stdrun.hoc')
    h.t = 0.
    h.dt = 0.02
    h.v_init = -69.
    h.celsius = 37.
    h.tstop = ttran + 100

    h.finitialize(h.v_init)

    print('Running the model...')
    sys.stdout.flush()
    h.run()

    found = False
    while h.tstop <= tstop:
        t = np.array(rec['t'])
        v = np.array(rec['vsoma'])
        spikes = np.array(rec['spikes'])
        spikes = spikes[spikes>ttran]
        isi = np.diff(spikes)
        try:
            idx = np.where((1000./isi >= firing_rate-2.) & (1000./isi <= firing_rate+2))[0][which_isi]
            found = True
            break
        except:
            print('No spikes @ %g Hz in the first %g ms. Continuing...' % (firing_rate,h.tstop))
            h.tstop += 100.
            h.continuerun(h.tstop)

    if not found:
        print('Unable to find the right ISI in the first %g ms.' % h.tstop)
        sys.exit(0)

    tbefore = 2.
    tafter = 5.
    t0 = spikes[idx]
    t1 = spikes[idx+1]
    idx = int(t0/h.dt) + np.argmax(v[int(t0/h.dt):int((t0+2)/h.dt)])
    t0 = t[idx]
    idx = int(t1/h.dt) + np.argmax(v[int(t1/h.dt):int((t1+2)/h.dt)])
    t1 = t[idx]
    print('Good ISI found between %g and %g ms.' % (t0,t1))

    print('Adding all other objects...')
    rs = np.random.RandomState(int(time.time()))
    for j in range(1,ntrials):
        np.random.seed(seed)
        for k,syn in enumerate(it.chain(cell.synapses['granule_cells'],cell.synapses['stellate_cells'])):
            s,st,nc = cell.insert_synapse(syn['sec'], syn['tau'], syn['E'], 0.)
            if syn in cell.synapses['granule_cells']:
                isi = -np.log(np.random.uniform(size=len(syn['spike_times'][0]))) / exc_rate * 1e3
            else:
                isi = -np.log(np.random.uniform(size=len(syn['spike_times'][0]))) / inh_rate * 1e3
            spks = np.cumsum(isi)            
            idx, = np.where((spks>t0) & (spks<t1))
            if len(idx) > 0:
                isi[idx] += rs.uniform(size=len(idx))
            spks = np.cumsum(isi)
            vec = h.Vector(spks)
            st.play(vec)
            syn['syn'].append(s)
            syn['stim'].append(st)
            syn['conn'].append(nc)
            syn['spike_times'].append(vec)

    print('Running the model again...')
    sys.stdout.flush()
    rec['t'].resize(0)
    rec['vsoma'].resize(0)
    rec['spikes'].resize(0)
    apc.n = 0
    h.t = 0

    h.tstop = t0 - tbefore
    h.finitialize(h.v_init)
    h.run()

    print('Saving the state...')
    ss = h.SaveState()
    ss.save()

    h.dt = 0.005
    nsamples = np.round((t1-t0+tbefore+tafter) / h.dt)
    V = np.zeros((ntrials,nsamples))
    spike_times = np.nan + np.zeros((ntrials,2))
    perturbation_times = np.zeros(ntrials)
    for i in range(ntrials):
        sys.stdout.write('\rTrial [%02d/%02d] ' % (i+1,ntrials))
        sys.stdout.flush()
        ss.restore()
        rec['t'].resize(0)
        rec['vsoma'].resize(0)
        rec['spikes'].resize(0)
        apc.n = 0
        for syn in it.chain(cell.synapses['granule_cells'],cell.synapses['stellate_cells']):
            for j in range(ntrials):
                syn['conn'][j].weight[0] = 0
            syn['conn'][i].weight[0] = syn['w']
        if i > 0:
            pulse.delay = t0 + i*(t1+5-t0)/ntrials
        perturbation_times[i] = pulse.delay
        h.continuerun(t1+tafter+1)
        idx, = np.where(np.array(rec['spikes']) > t0-tbefore)
        try:
            spike_times[i,:] = np.array(rec['spikes'])[idx[0]:idx[0]+2]
        except:
            pass
        V[i,:] = np.array(rec['vsoma'])[:nsamples]
    t = np.array(rec['t'])[idx[0]:idx[0]+nsamples]

    #p.figure()
    #for i in range(ntrials):
    #    p.plot(t,V[i,:])
    #p.show()

    return t,V,perturbation_times,spike_times,presyn_spike_times
    
def main():

    print('Building model...')
    if sys.argv[1] == 'iclamp':
        cell = DS2M0Purk(None,None)
        # Somatic current injection
        amplitude = 0.2
        tbefore = 10
        tstim = 10000.
        tafter = 100.
        celsius = 37.
        t,vsoma,vdend,spikes = somatic_current_injection(cell, amplitude, tbefore, tstim, tafter, celsius)
        tbefore *= 1e-3
        tstim *= 1e-3
        print('Firing rate: {0} Hz.'.format(len(np.intersect1d(np.where(spikes>=tbefore+1)[0],\
                                                                   np.where(spikes<=tbefore+tstim)[0]))/(tstim-1)))
    elif sys.argv[1] == 'synapses':
        # Dendritic synapse activation
        g_granule = 0.7e-3
        g_stellate = [7000.,1400.]
        cell = DS2M0Purk(g_granule, g_stellate)
        t,vsoma,vdend,spikes = synaptic_activation(cell,exc_rate=35.,inh_rate=2, tstop=5000., seed=5061983)
        save_h5_file(make_output_filename('synaptic_activation_','.h5'), dt=t[1]-t[0], V=vsoma)
    elif sys.argv[1] == 'PRC':
        # Computation of the PRC
        g_granule = 0.7e-3
        g_stellate = [7000.,1400.]
        cell = DS2M0Purk(g_granule, g_stellate)
        exc_rate = 35.
        inh_rate = 2.
        try:
            firing_rate = float(sys.argv[2])
        except:
            firing_rate = 85.
        print('Target firing rate: %.1f Hz.' % firing_rate)
        try:
            pulse_amp = float(sys.argv[3])
        except:
            pulse_amp = 0.5
        print('Pulse amplitude: %.2f nA.' % pulse_amp)
        which_isi = 0
        ttran = 3900.
        tstop = 5000.
        ntrials = 100
        seed = 5061983
        pulse_dur = 0.5
        t,V,perturbation_times,spike_times,presyn_spike_times = \
            compute_PRC(cell, exc_rate, inh_rate, firing_rate, seed, which_isi, ttran, tstop, ntrials, pulse_dur, pulse_amp)
        save_h5_file(make_output_filename('prc_','.h5'),dt=t[1]-t[0],V=V,perturbation_times=perturbation_times,
                     spike_times=spike_times,exc_rate=exc_rate,inh_rate=inh_rate,firing_rate=firing_rate,
                     which_isi=which_isi,ttran=ttran,tstop=tstop,ntrials=ntrials,g_granule=g_granule,g_stellate=g_stellate,
                     presyn_spike_times=presyn_spike_times,seed=seed,pulse_amp=pulse_amp,pulse_dur=pulse_dur)

    try:
        p.figure()
        p.plot(t,vsoma,'k',label='Soma')
        p.plot(t,vdend,'r',label='Dendrite')
        p.xlabel('Time (s)')
        p.ylabel('Membrane voltage (mV)')
        p.legend(loc='best')
        p.figure()
        p.plot(spikes[1:],1./np.diff(spikes),'ko')
        p.xlabel('Time (s)')
        p.ylabel('1/ISI (s^-1)')
        p.show()
    except:
        pass

if __name__ == '__main__':
    main()
